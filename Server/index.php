<?php

if(isset($_POST))
{
	list($action, $resource, $folder) = explode('/', substr($_SERVER['REQUEST_URI'], 1));

	switch($action)
	{
		case 'set':

			switch($resource)
			{
				case 'json':

					if(!empty($_POST['name']) and !empty($_POST['json']))
					{
						file_put_contents("$folder/{$_POST['name']}.conf", "{$_POST['json']}\n");
					}

					break;
			}

			break;

		case 'get':

			switch($resource)
			{
				case 'json':

					if(!empty($_POST['name']))
					{
						$file = "$folder/{$_POST['name']}.conf";
						if(file_exists($file))
						{
							$json = file_get_contents($file);

							echo $json;
						}
					}

					break;

				case 'files':

					$files = glob("$folder/*.conf", GLOB_NOSORT);
					if(!empty($files))
					{
						$json = json_encode($files);

						echo $json;
					}

					break;
			}

			break;

		case 'new':

			switch($resource)
			{
				case 'json':

					if(!empty($_POST['name']))
					{
						$temp = "$folder/{$_POST['name']}.temp";
						if(file_exists($temp))
						{
							$file = "$folder/{$_POST['name']}.conf";
							if(file_exists($file) and filemtime($file) > filemtime($temp))
							{
								$json = file_get_contents($file);

								echo $json;
							}
						}
						touch($temp);
					}

					break;

				case 'files':

					$temp = "$folder/files.temp";
					if(file_exists($temp))
					{
						$files = glob("$folder/*.conf", GLOB_NOSORT);
						if(!empty($files))
						{
							$array = array();
							foreach($files as $file)
							{
								if(filemtime($file) > filemtime($temp))
								{
									$array[] = $file;
								}
							}

							if(!empty($array))
							{
								$json = json_encode($array);

								echo $json;
							}
						}
					}
					touch($temp);

					break;
			}

			break;
	}
}