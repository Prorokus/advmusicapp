package com.advmusicapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.advmusicapp.ClientApp;
import com.advmusicapp.MainClientActivity;
import com.advmusicapp.helpers.ClientHelper;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;

/**
 * Created with IntelliJ IDEA.
 * User: vladya
 * Date: 2/14/13
 * Time: 5:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class AlarmRebootReceiver extends BroadcastReceiver {
    public static String REBOOT_ALARM = "com.advmusicapp.client.REBOOT_ALARM";

    @Override
    public void onReceive(Context context, Intent intent) {
    	EasyTracker.getInstance().setContext(context);
    	
        Tracker myTracker = EasyTracker.getTracker();      // Get a reference to tracker.
        myTracker.sendEvent("Client", "Rebooting", "onReceive", 0l);
    	
    	if (MainClientActivity.mp != null && MainClientActivity.mp.isPlaying())
    	{
            SharedPreferences settings;
            settings = context.getSharedPreferences("AVD_PREF", Context.MODE_PRIVATE);

            SharedPreferences.Editor editor = settings.edit();

            editor.putString("isPlaying", "true");

            editor.commit();    		
    	}
    	
    	try {
            Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot" });
            proc.waitFor();
        } catch (Exception ex) {
            Log.i("AdvMusic Client", "Could not reboot", ex);
            myTracker.sendEvent("Client", "Rebooting", ex.toString(), 0l);
            
            FlurryAgent.onError("Catch", ex.getMessage(), "AlarmRebootReceiver - " + "52");
        }   
    }
}
