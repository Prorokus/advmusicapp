package com.advmusicapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.advmusicapp.MainClientActivity;

/**
 * Created with IntelliJ IDEA. User: vladya Date: 2/14/13 Time: 5:24 PM To change this template use
 * File | Settings | File Templates.
 */
public class AlarmStopReceiver extends BroadcastReceiver {
  public static String STOP_ALARM = "com.advmusicapp.client.STOP_ALARM";

  @Override
  public void onReceive(final Context context, final Intent intent) {
    /*
     * long diff = intent.getLongExtra("diff",0);
     * 
     * if(diff < 0) return;
     */

    if (MainClientActivity.instance != null) {
      MainClientActivity.instance.stopSong(false);
    }
  }
}
