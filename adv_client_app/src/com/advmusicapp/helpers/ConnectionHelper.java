package com.advmusicapp.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class ConnectionHelper 
{
    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        
        if (connectivity == null) {
            return false;
        } 
        
        else 
        {
        	NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
        	if (networkInfo != null)
        	{
	        	State networkState = networkInfo.getState();
	        	
	        	if (networkState == NetworkInfo.State.CONNECTED)
	            {
	                return true;
	            }
        	}
        }
        
        return false;
    }
}
