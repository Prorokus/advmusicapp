package com.advmusicapp.helpers;

import android.content.Context;

import com.advmusicapp.objects.ClientData;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 12.02.13 Time: 1:15 To change this template use File
 * | Settings | File Templates.
 */
public class ClientHelper {

  public static ClientData clientData;

  /**
   * Send current city and name to web server
   * 
   */
  public static void updateMainData(final String city, final String name) {

    if (!clientData.getCity().equals(city) || !clientData.getName().equals(name)) {
      clientData.setCity(city);
      clientData.setName(name);

      DropBoxHelper.getInstance().writeConfigFile(clientData.getName(), clientData.getJSON());
    }
  }

  /**
   * Send now playing song to web server
   * 
   */
  public static void updateNowPlayingData(final Context c) {
    if (!ConnectionHelper.isNetworkAvailable(c)) {
      return;
    }

    DropBoxHelper.getInstance().UpdateNowPlayingData(clientData.getName(),
        clientData.getNowPlayingJSON());
  }
}
