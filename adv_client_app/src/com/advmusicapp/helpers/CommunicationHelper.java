package com.advmusicapp.helpers;

import com.advmusicapp.ClientApp;
import com.advmusicapp.Constants;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Tracker;
import com.tylersmith.net.RequestMethod;
import com.tylersmith.net.RestClient;

/**
 * Static functions that are responsible for all communications with the server
 * 
 * @author Oleg Lytovchenko
 */

public class CommunicationHelper {
  /**
   * Get base RestClient
   * 
   * @param url
   *          url for sending data
   * @return RestClient
   * */
  private static RestClient getCustomClient(final String url) {
    EasyTracker.getInstance().setContext(ClientApp.getInstance().getApplicationContext());

    return new RestClient(url);
  }

  public static boolean CheckConnection() {
    final RestClient restClient = getCustomClient(Constants.WRITE_CONFIG_URL);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return true;
      }

    } catch (final Exception e) {
      // e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "60");
    }

    return false;
  }

  /**
   * Send config file on web server
   * 
   * @param name
   *          - client name converted in md5
   * @param json
   *          - client data
   * @return true if no errors
   */
  public static boolean WriteConfigFile(final String name, final String json) {
    final RestClient restClient = getCustomClient(Constants.WRITE_CONFIG_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.

    if (myTracker != null) {
      myTracker.sendEvent("Client", "WriteConfigFile", name, 0l);
    }

    restClient.addParam("name", name);
    restClient.addParam("json", json);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return true;
      }

    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "93");
      // e.printStackTrace();
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return false;
  }

  /**
   * Send now playing info file on web server
   * 
   * @param name
   *          - client name converted in md5
   * @param json
   *          - client data
   * @return true if no errors
   */
  public static boolean WriteNowPLayingFile(final String name, final String json) {
    final RestClient restClient = getCustomClient(Constants.WRITE_PLAYING_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.

    if (myTracker != null) {
      myTracker.sendEvent("Client", "WriteNowPLayingFile", name, 0l);
    }

    restClient.addParam("name", name);
    restClient.addParam("json", json);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return true;
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "128");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return false;
  }

  /**
   * Get config file from web server
   * 
   * @param name
   *          - client name converted in md5
   * @return JSON if success, empty string if error
   */
  public static String readConfigFile(final String name) {
    final RestClient restClient = getCustomClient(Constants.READ_CONFIG_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.

    if (myTracker != null) {
      myTracker.sendEvent("Client", "readConfigFile", name, 0l);
    }

    restClient.addParam("name", name);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "161");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get now playing music info file from web server
   * 
   * @param name
   *          - client name converted in md5
   * @return JSON if success, empty string if error
   */
  public static String readNowPLayingFile(final String name) {
    final RestClient restClient = getCustomClient(Constants.READ_PLAYING_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "readNowPLayingFile", name, 0l);
    }

    restClient.addParam("name", name);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "193");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get all config file names (md5) list from web server
   * 
   * @return JSON if success, empty string if error
   */
  public static String getAllConfigsList() {
    final RestClient restClient = getCustomClient(Constants.GET_CONFIGS_LIST_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "getAllConfigsList", "", 0l);
    }

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "223");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get all config file names (md5) list from web server
   * 
   * @return JSON if success, empty string if error
   */
  public static String getAllSongsList() {
    final RestClient restClient = getCustomClient(Constants.GET_SONGS_LIST_URL);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "204");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get all now playing song info files names (md5) list from web server
   * 
   * @return JSON if success, empty string if error
   */
  public static String getAllNowPLayingList() {
    final RestClient restClient = getCustomClient(Constants.GET_PLAYING_LIST_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "getAllNowPLayingList", "", 0l);
    }

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "253");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get config file JSON from web server, if it was changed from last attempt to get it
   * 
   * @param name
   *          - client name converted in md5
   * @return JSON if success, empty string if error or if it was'nt changed
   */
  public static String getChangedConfigFile(final String name) {
    final RestClient restClient = getCustomClient(Constants.GET_NEW_CONFIG_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "getChangedConfigFile", name, 0l);
    }

    restClient.addParam("name", name);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "285");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get now playing info JSON from web server, if it was changed from last attempt to get it
   * 
   * @param name
   *          - client name converted in md5
   * @return JSON if success, empty string if error or if it was'nt changed
   */
  public static String getChangedNowPlayingFile(final String name) {
    final RestClient restClient = getCustomClient(Constants.GET_NEW_PLAYING_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "getChangedNowPlayingFile", name, 0l);
    }

    restClient.addParam("name", name);

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "317");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get JSON list of file names (md5) of config files from web server (files that was changed from
   * last attempt to getting them)
   * 
   * @return JSON if success, empty string if error or if no any file changed
   */
  public static String getAllChangedConfigsList() {
    final RestClient restClient = getCustomClient(Constants.GET_NEW_CONFIG_LIST_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "getAllChangedConfigsList", "", 0l);
    }

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "347");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }

  /**
   * Get JSON list of file names (md5) of now playing infi files from web server (files that was
   * changed from last attempt to getting them)
   * 
   * @return JSON if success, empty string if error or if no any file changed
   */
  public static String getAllChangedNowPLayingList() {
    final RestClient restClient = getCustomClient(Constants.GET_NEW_PLAYING_LIST_URL);

    final Tracker myTracker = EasyTracker.getTracker(); // Get a reference to tracker.
    if (myTracker != null) {
      myTracker.sendEvent("Client", "getAllChangedNowPLayingList", "", 0l);
    }

    try {
      restClient.execute(RequestMethod.POST);
      if (restClient.getResponseCode() == 200) {
        // Successfully connected
        return restClient.getResponse();
      }
    } catch (final Exception e) {
      if (myTracker != null) {
        myTracker.sendException(e.getMessage(), false); // false indicates non-fatal exception.
      }

      FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "377");
    }

    DropBoxHelper.showToastMsg("Error sending data");

    return "";
  }
}
