package com.advmusicapp.helpers;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;

import com.advmusicapp.Constants;
import com.advmusicapp.objects.FileObject;
import com.flurry.android.FlurryAgent;

/**
 * DropBox API was here in past. Now songs synchronization is managed by dropSync program. And
 * config files synchronization is managed by web server.
 * 
 * This class is just helps to have conversation with CommunicationHelper. Also this class checks
 * for config files updates on web server
 * 
 * @author Oleg Lytovchenko
 */

public class DropBoxHelper {
  // Parrent activity (to work with fragments)
  private static Activity parrentActivity;

  // Handler to main UI thread
  public static Handler mHandler = new Handler();

  private static DropBoxHelper instance;

  private final Timer updateTimer = new Timer();
  private TimerTask tS;
  private TimerTask tS2;

  private Thread updateNowPlayingDataThread;

  private final Context mContext;

  private List<String> mWebAudioDirList = Collections.synchronizedList(new ArrayList<String>());

  private long downloadId = 0;

  public static DropBoxHelper getInstance() {
    return instance;
  }

  public DropBoxHelper(final Context context) {
    mContext = context;
    instance = this;
  }

  /**
   * Reset timer when exiting program
   * 
   */
  public void kill() {
    if (tS != null) {
      tS.cancel();
    }

    if (tS2 != null) {
      tS2.cancel();
    }
  }

  /**
   * Change parrent activity
   * 
   */
  public void setActivity(final Activity act) {
    parrentActivity = act;
  }

  /**
   * Init timer to check new config data on web server every 35 seconds
   * 
   */
  public void init() {
    tS = new TimerTask() {
      @Override
      public void run() {
        updateConfigData();
      }
    };

    tS2 = new TimerTask() {
      @Override
      public void run() {
        // Get files list from server
        updateAudioData();
      }
    };

    updateTimer.schedule(tS, 35 * 1000);
    updateTimer.schedule(tS2, 120 * 1000);
  }

  public void showOkBox(final String msg) {
    /*
     * AlertDialog.Builder dlgAlert = new AlertDialog.Builder(parrentActivity);
     * 
     * dlgAlert.setMessage(parrentActivity.getString(R.string.info_sent) + ": " + msg);
     * dlgAlert.setTitle(parrentActivity.getString(R.string.app_name));
     * dlgAlert.setPositiveButton("OK", null); dlgAlert.setCancelable(true);
     * dlgAlert.create().show();
     * 
     * dlgAlert.setPositiveButton("Ok", new DialogInterface.OnClickListener() { public void
     * onClick(DialogInterface dialog, int which) { } });
     */
  }

  public static void showToastMsg(final String msg) {
    mHandler.post(new Runnable() {
      @Override
      public void run() {
        Toast.makeText(parrentActivity, msg, Toast.LENGTH_SHORT).show();
      }
    });
  }

  /**
   * Generate md5 string from city name (all config and nowPlaying files are stored under md5 names
   * at web server) this md5 strings generated from current client names in every city
   */
  public static String getMd5FromString(final String s) {
    MessageDigest m = null;

    try {
      m = MessageDigest.getInstance("MD5");
      m.update(s.getBytes(), 0, s.length());
      return new BigInteger(1, m.digest()).toString(16);
    } catch (final NoSuchAlgorithmException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();

      FlurryAgent.onError("Catch", e.getMessage(), "DropBoxHelper - " + "108");
    }

    return "";
  }

  /**
   * Send check new data event to main activity and restart timer
   * 
   */
  private void updateAudioData() {
    tS2 = new TimerTask() {
      @Override
      public void run() {
        updateAudioData();
      }
    };

    updateTimer.purge();
    updateTimer.schedule(tS2, 120 * 1000);

    final DownloadManager manager = (DownloadManager) parrentActivity
        .getSystemService(Context.DOWNLOAD_SERVICE);
    final Cursor c = manager.query(new DownloadManager.Query().setFilterById(downloadId));
    boolean isReadyToDownload = true;

    if (c.moveToFirst()) {
      do {
        final int data = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));

        if (data == DownloadManager.STATUS_RUNNING || data == DownloadManager.STATUS_PENDING) {
          isReadyToDownload = false;
        }

      } while (c.moveToNext());
    }
    c.close();

    if (isReadyToDownload) {
      // new GetAudioFilesListFromWeb().execute();
      mWebAudioDirList = getAudioFilesInfo(CommunicationHelper.getAllSongsList());
      updateAudioFiles();
    }
  }

  /**
   * Send check new data event to main activity and restart timer
   * 
   */
  private void updateConfigData() {
    tS = new TimerTask() {
      @Override
      public void run() {
        updateConfigData();
      }
    };

    updateTimer.purge();
    updateTimer.schedule(tS, 35 * 1000);

    if (!ConnectionHelper.isNetworkAvailable(parrentActivity)) {
      return;
    }

    final Intent intent = new Intent();
    intent.setAction("com.advmusicapp.configFilesModified");
    parrentActivity.sendBroadcast(intent);
  }

  /**
   * Parce array with files received from file system to receive List with their names without
   * extensions
   * 
   * @return List<String> with file names (parsed sdAudioDirList)
   */
  public List<String> getAudioFilesInfo(final String list) {
    List<String> res = new ArrayList<String>();

    if (list.equals("")) {
      return res;
    }

    String strToParse = new String(list);

    while (strToParse.length() > 0) {
      int indexStart = strToParse.indexOf("audio\\/");
      int indexEnd = strToParse.indexOf("audio\\/", indexStart + 1);

      if (indexStart == -1) {
        res = fixUnicodeNames(res);
        return res;
      } else {
        indexStart += 6;
      }

      if (indexEnd == -1) {
        indexEnd = strToParse.length() - 3;
      } else {
        indexEnd -= 3;
      }

      res.add(strToParse.substring(indexStart + 1, indexEnd));

      strToParse = strToParse.substring(indexEnd, strToParse.length());
    }

    res = fixUnicodeNames(res);
    return res;
  }

  private List<String> fixUnicodeNames(final List<String> paramList) {
    final List<String> resList = new ArrayList<String>();

    for (String s : paramList) {
      if (s.contains("\\u")) {
        // res.remove(s);
        String newS = "";

        while (s.length() > 0) {
          if (!s.startsWith("\\")) {
            newS += s.substring(0, 1);
            s = s.substring(1, s.length());
          } else {
            s = s.substring(2, s.length());
            final String convert = s.substring(0, 4);
            s = s.substring(4, s.length());
            final int hexVal = Integer.parseInt(convert, 16);
            newS += (char) hexVal;
          }
        }

        resList.add(newS);
      }

      else {
        resList.add(s);
      }
    }

    return resList;
  }

  /*
   * public class GetAudioFilesListFromWeb extends AsyncTask<String, Void, Void> {
   * 
   * @Override protected void onPreExecute() { }
   * 
   * @Override protected synchronized Void doInBackground(final String... strings) {
   * mWebAudioDirList = getAudioFilesInfo(CommunicationHelper.getAllSongsList()); return null; }
   * 
   * @Override protected void onPostExecute(final Void v) { mHandler.post(new Runnable() {
   * 
   * @Override public void run() { updateAudioFiles(); } }); } }
   */

  /**
   * @param context
   *          used to check the device version and DownloadManager information
   * @return true if the download manager is available
   */
  public static boolean isDownloadManagerAvailable(final Context context) {
    try {
      if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
        return false;
      }
      final Intent intent = new Intent(Intent.ACTION_MAIN);
      intent.addCategory(Intent.CATEGORY_LAUNCHER);
      intent.setClassName("com.android.providers.downloads.ui",
          "com.android.providers.downloads.ui.DownloadList");
      final List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
          PackageManager.MATCH_DEFAULT_ONLY);
      return list.size() > 0;
    } catch (final Exception e) {
      return false;
    }
  }

  private void updateAudioFiles() {
    showToastMsg("Updating audio");
    final File myFile = new File(Environment.getExternalStorageDirectory() + "/AdvMusic/audio/");
    final File[] oldAudioDirList = myFile.listFiles();

    if (oldAudioDirList == null) {
      return;
    }

    if (mWebAudioDirList.size() == 0) {
      return;
    }

    // Delete old files
    for (final File inf : oldAudioDirList) {
      if (!mWebAudioDirList.contains(inf.getName())) {
        inf.delete();
      }
    }

    // Download new files
    for (final String name : mWebAudioDirList) {
      boolean contains = false;

      for (final File inf : oldAudioDirList) {
        if (inf.getName().equals(name)) {
          contains = true;
        }
      }

      if (!contains && isDownloadManagerAvailable(parrentActivity)) {
        final String url = Constants.AUDIO_DOWNLOAD_URL + Uri.encode(name);
        final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription("AdvMusic audio download");
        request.setTitle(name);

        request.setDestinationInExternalPublicDir("/AdvMusic/audio/", name);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) parrentActivity
            .getSystemService(Context.DOWNLOAD_SERVICE);
        downloadId = manager.enqueue(request);
      }
    }
  }

  /**
   * Try to push current song name on server in separate thread
   * 
   * @param name
   *          - name of the current client
   * @param inf
   *          - shortened JSON with current song name and some more data
   */
  public void UpdateNowPlayingData(final String name, final JSONObject inf) {
    if (updateNowPlayingDataThread != null && updateNowPlayingDataThread.isAlive()) {
      return;
    }

    updateNowPlayingDataThread = new Thread(new Runnable() {
      @Override
      public void run() {
        writeCurrentSongFile(name, inf);
      }
    }, "Writing now playing data");
    updateNowPlayingDataThread.start();
  }

  /**
   * Write nowPlaying song file on web server
   * 
   */
  public void writeCurrentSongFile(String name, final JSONObject inf) {
    if (name.endsWith(".conf")) {
      name = name.replace(".conf", "");
    }

    final String md5Name = getMd5FromString(name);

    CommunicationHelper.WriteNowPLayingFile(md5Name, inf.toString());
  }

  /**
   * Write config file for current client on web server
   * 
   */
  public void writeConfigFile(String name, final JSONObject inf) {
    if (name.endsWith(".conf")) {
      name = name.replace(".conf", "");
    }

    final String md5Name = getMd5FromString(name);

    CommunicationHelper.WriteConfigFile(md5Name, inf.toString());
  }

  /**
   * Read config file from server
   * 
   * @param name
   *          - current client name
   * @param respectChanges
   *          - if false = just read file, if true = read file only if it was changed. In other way
   *          getChangedConfigFile will return empty JSON
   * @return JSON with asked file
   */
  public JSONObject readConfigFile(String name, final boolean respectChanges) {
    if (name == null) {
      return null;
    }

    if (name.endsWith(".conf")) {
      name = name.replace(".conf", "");
    }

    String aBuffer = "";

    if (respectChanges) {
      aBuffer = CommunicationHelper.getChangedConfigFile(getMd5FromString(name));
    } else {
      aBuffer = CommunicationHelper.readConfigFile(getMd5FromString(name));
    }

    aBuffer = aBuffer.replace("\\", "");

    JSONObject j = null;

    if (!aBuffer.equals("")) {
      try {
        j = new JSONObject(aBuffer);
      } catch (final JSONException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();

        FlurryAgent.onError("Catch", e.getMessage(), "DropBoxHelper - " + "200");
      }
    }

    return j;
  }

  /**
   * Try to open file on sd card at /AdvMusic/audio/ folder
   * 
   * @return object with file descriptor
   */
  public FileObject readAudioFile(String name) {
    final FileObject fObj = new FileObject();

    if (!name.endsWith(".mp3")) {
      name += ".mp3";
    }

    name = Environment.getExternalStorageDirectory() + "/AdvMusic/audio/" + name;

    ParcelFileDescriptor pfd;
    FileDescriptor fd = null;
    final File f = new File(name);

    try {
      pfd = ParcelFileDescriptor.open(f, ParcelFileDescriptor.MODE_READ_ONLY);
      fd = pfd.getFileDescriptor();
    } catch (final FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();

      FlurryAgent.onError("Catch", e.getMessage(), "DropBoxHelper - " + "229");
    }

    fObj.setFd(fd);

    return fObj;
  }
}