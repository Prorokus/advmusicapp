package com.advmusicapp.objects;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import com.advmusicapp.helpers.ClientHelper;
import com.flurry.android.FlurryAgent;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 11.02.13 Time: 23:36 To change this template use File
 * | Settings | File Templates.
 */
public class ClientData {
  private String uuid;
  private String city;
  private String name;
  private List<String> audioFiles;
  private List<String> failedToPlayFiles;
  private static HashMap<String, ArrayList<Long>> audioFilesTimeToPlay;
  private String startTime;
  private String stopTime;
  private int songStatus; // (-1) - nothing 0 - stop 1 - start
  private int volume;
  private String nowPlaying = "";

  private boolean dontPlayFlag = false;

  private final int version = 21;

  public ClientData() {
  }

  public ClientData(final Context context) {
    final TelephonyManager tManager = (TelephonyManager) context
        .getSystemService(Context.TELEPHONY_SERVICE);

    if (tManager.getDeviceId() != null) {
      uuid = tManager.getDeviceId();
    } else {
      uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    city = "";
    name = "";
    audioFiles = new ArrayList<String>();
    audioFilesTimeToPlay = new HashMap<String, ArrayList<Long>>();
    startTime = "7:30";
    stopTime = "21:00";
    songStatus = -1;
    volume = 1;
    nowPlaying = "";
    dontPlayFlag = false;
  }

  public List<String> getFailedToPlayFiles() {
    if (failedToPlayFiles == null) {
      failedToPlayFiles = new ArrayList<String>();
    }

    return failedToPlayFiles;
  }

  public boolean getDontPlayFlag() {
    return dontPlayFlag;
  }

  public void setDontPlayFlag(final boolean b) {
    dontPlayFlag = b;
  }

  public String getUuid() {
    return uuid;
  }

  public String getCity() {
    return city;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<String> getAudioFiles() {
    final List<String> l = new ArrayList<String>();

    for (final String s : this.audioFiles) {
      l.add(s);
    }

    return l;
  }

  // public void setAudioFiles(final List<String> audioFiles) {
  // this.audioFiles.clear();
  //
  // for (final String s : audioFiles) {
  // this.audioFiles.add(s);
  // }
  // }

  public HashMap<String, ArrayList<Long>> getAudioFilesTimeToPlay() {
    if (audioFilesTimeToPlay == null) {
      audioFilesTimeToPlay = new HashMap<String, ArrayList<Long>>();

    }

    return audioFilesTimeToPlay;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(final String startTime) {
    this.startTime = startTime;
  }

  public String getStopTime() {
    return stopTime;
  }

  public void setStopTime(final String stopTime) {
    this.stopTime = stopTime;
  }

  public int getSongStatus() {
    return songStatus;
  }

  public void setSongStatus(final int songStatus) {
    this.songStatus = songStatus;
  }

  public int getVolume() {
    return volume;
  }

  public void setVolume(final int volume) {
    this.volume = volume;
  }

  public String getNowPlaying() {
    return nowPlaying;
  }

  public void setNowPlaying(final String nowPlaying) {
    this.nowPlaying = nowPlaying;
  }

  public JSONObject getJSON() {
    final JSONObject json = new JSONObject();
    try {
      json.put("version", version);
      json.put("uuid", uuid);
      json.put("city", city);
      json.put("name", name);
      json.put("audioFiles", new JSONArray(audioFiles));
      if (audioFilesTimeToPlay != null) {
        json.put("audioFilesTimeToPlay", new JSONObject(audioFilesTimeToPlay));
      }
      json.put("startTime", startTime);
      json.put("stopTime", stopTime);
      json.put("songStatus", songStatus);
      json.put("volume", volume);
      json.put("nowPlaying", nowPlaying);

      json.put("dontPlayFlag", dontPlayFlag);

    } catch (final JSONException e) {
      e.printStackTrace();

      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "150");
    }

    return json;
  }

  /**
   * Shortened version of JSON data (used to send current song on server)
   * 
   */

  public JSONObject getNowPlayingJSON() {
    final JSONObject json = new JSONObject();
    try {
      json.put("version", version);
      json.put("uuid", uuid);
      json.put("city", city);
      json.put("name", name);
      json.put("songStatus", songStatus);
      json.put("volume", volume);
      json.put("nowPlaying", nowPlaying);
      json.put("failedToPlayFiles", failedToPlayFiles);
    } catch (final JSONException e) {
      e.printStackTrace();

      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "168");
    }

    return json;
  }

  public boolean compareJSONAndModel(final JSONObject j1) {
    String s = "";
    boolean res = true;

    if (j1 == null) {
      return false;
    }

    try {
      s = j1.getString("uuid");
      if (!s.equals(this.uuid)) {
        res = false;
      }

      s = j1.getString("city");
      if (!s.equals(this.city)) {
        res = false;
      }

      s = j1.getString("name");
      if (!s.equals(this.name)) {
        res = false;
      }

      final JSONArray jArr = j1.getJSONArray("audioFiles");
      if (jArr != null) {
        final int len = jArr.length();

        if (len == this.audioFiles.size()) {
          for (int i = 0; i < len; i++) {
            if (!jArr.get(i).toString().equals(this.audioFiles.get(i))) {
              res = false;
            }
          }
        }

        else {
          res = false;
        }
      }

      s = j1.getString("startTime");
      if (!s.equals(this.startTime)) {
        res = false;
      }

      s = j1.getString("stopTime");
      if (!s.equals(this.stopTime)) {
        res = false;
      }

      int i = j1.getInt("songStatus");
      if (i != this.songStatus) {
        res = false;
      }

      i = j1.getInt("volume");
      if (i != this.volume) {
        res = false;
      }

      final boolean b = j1.getBoolean("dontPlayFlag");
      if (b != this.dontPlayFlag) {
        res = false;
      }

      /*
       * s = j1.getString("nowPlaying"); if (!s.equals(this.nowPlaying)) { res = false; }
       */

    } catch (final JSONException e) {
      e.printStackTrace();

      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "256");
    }

    return res;
  }

  /**
   * Generate new ClientData object from JSON
   * 
   */
  public static void fromJSON(final JSONObject json) {
    try {
      if (!json.isNull("uuid") && !json.getString("uuid").isEmpty()) {
        ClientHelper.clientData.uuid = json.getString("uuid");
      }
      if (!json.isNull("city") && !json.getString("city").isEmpty()) {
        ClientHelper.clientData.city = json.getString("city");
      }
      if (!json.isNull("name") && !json.getString("name").isEmpty()) {
        ClientHelper.clientData.name = json.getString("name");
      }

      if (!json.isNull("audioFiles") && !json.getString("name").isEmpty()) {
        final JSONArray jsonArray = json.getJSONArray("audioFiles");

        if (jsonArray != null) {
          ClientHelper.clientData.audioFiles = new ArrayList<String>();
          final int len = jsonArray.length();
          for (int i = 0; i < len; i++) {
            ClientHelper.clientData.audioFiles.add(jsonArray.get(i).toString());
          }
        }
      }

      if (!json.isNull("audioFilesTimeToPlay") && !json.getString("name").isEmpty()) {
        final JSONObject jsonObj = json.getJSONObject("audioFilesTimeToPlay");

        if (jsonObj != null) {
          final Iterator<?> keys = jsonObj.keys();
          audioFilesTimeToPlay = new HashMap<String, ArrayList<Long>>();

          while (keys.hasNext()) {
            final String key = (String) keys.next();
            final String mStringArray = jsonObj.getString(key).substring(1,
                jsonObj.getString(key).length() - 1);

            final Scanner s = new Scanner(mStringArray);
            s.useDelimiter(", ");
            final ArrayList<Long> list = new ArrayList<Long>();
            while (s.hasNext()) {
              // Transform time to date and time!
              final String timeToParce = s.next();
              final int timeIndex = timeToParce.indexOf(":");

              if (timeIndex == -1) {
                continue;
              }

              final int hh = Integer.valueOf(timeToParce.substring(0, timeIndex));
              final int mm = Integer.valueOf(timeToParce.substring(timeIndex + 1,
                  timeToParce.length()));

              final Calendar c = Calendar.getInstance();

              c.set(Calendar.HOUR_OF_DAY, hh);

              if (hh <= 12) {
                c.set(Calendar.HOUR, hh);
                c.set(Calendar.AM_PM, 0);
              } else {
                c.set(Calendar.HOUR, hh - 12);
                c.set(Calendar.AM_PM, 1);
              }

              c.set(Calendar.MINUTE, mm);
              c.set(Calendar.SECOND, 0);
              c.set(Calendar.MILLISECOND, 0);
              long playTimeInMillis = c.getTimeInMillis();

              if (Calendar.getInstance().getTimeInMillis() >= playTimeInMillis) {
                playTimeInMillis += 86400000L;
              }

              list.add(playTimeInMillis);
            }
            audioFilesTimeToPlay.put(key, list);

          }
        }
      }

      if (!json.isNull("startTime") && !json.getString("startTime").isEmpty()) {
        ClientHelper.clientData.startTime = json.getString("startTime");
      } else {
        ClientHelper.clientData.startTime = "12:00";
      }
      if (!json.isNull("stopTime") && !json.getString("stopTime").isEmpty()) {
        ClientHelper.clientData.stopTime = json.getString("stopTime");
      } else {
        ClientHelper.clientData.stopTime = "12:10";
      }
      if (!json.isNull("songStatus") && !json.getString("songStatus").isEmpty()) {
        ClientHelper.clientData.songStatus = json.getInt("songStatus");
      } else {
        ClientHelper.clientData.songStatus = -1;
      }
      if (!json.isNull("volume") && !json.getString("volume").isEmpty()) {
        ClientHelper.clientData.volume = json.getInt("volume");
      } else {
        ClientHelper.clientData.volume = 50;
      }

      if (!json.isNull("dontPlayFlag") && !json.getString("dontPlayFlag").isEmpty()) {
        ClientHelper.clientData.dontPlayFlag = json.getBoolean("dontPlayFlag");
      } else {
        ClientHelper.clientData.dontPlayFlag = false;
      }

      /*
       * if(!json.isNull("nowPlaying")){ cData.nowPlaying = json.getString("nowPlaying"); }
       */
    } catch (final JSONException e) {
      e.printStackTrace();

      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "313");
    }
  }
}
