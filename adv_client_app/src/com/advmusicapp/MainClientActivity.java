package com.advmusicapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.json.JSONObject;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.advmusicapp.Constants.BuildType;
import com.advmusicapp.client.R;
import com.advmusicapp.helpers.ClientHelper;
import com.advmusicapp.helpers.DropBoxHelper;
import com.advmusicapp.objects.ClientData;
import com.advmusicapp.objects.FileObject;
import com.advmusicapp.receivers.AlarmRebootReceiver;
import com.advmusicapp.receivers.AlarmStartReceiver;
import com.advmusicapp.receivers.AlarmStopReceiver;
import com.everysoft.autoanswer.AutoAnswerIntentService;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 11.02.13 Time: 0:41 To change this template use File
 * | Settings | File Templates.
 */
public class MainClientActivity extends Activity {

  public static MainClientActivity instance;

  // private PowerManager.WakeLock wl;

  private String mediaPath;

  private ListView songsList;
  private SeekBar volume;
  private Button prev;
  private Button next;
  private Button playstop;
  private TextView playingNow;

  private List<String> songs = new ArrayList<String>();

  private CustomListAdapter songsAdapter;

  public static MediaPlayer mp;

  private AudioManager audioManager;

  private int currentPosition = -1;

  private DropBoxHelper mDropBoxHelper;

  private ConfigFilesModifiedReceiver mConfigFilesModifiedReceiver;

  private FileObject currentlyPlaying;

  private boolean audioInPause;
  private int songCurrentPosition;

  private int prevVolume = -1;

  private boolean afterBoot = false;

  private int errorPlayMusicCount = 0;

  private int storeCountSong = -1;

  private boolean isPlaying = false;
  private boolean isPlayingFromTimer = false;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (ClientHelper.clientData == null) {
      final Intent newIntent = new Intent(MainClientActivity.this, FirstLoadActivity.class);
      newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      newIntent.putExtra("afterBoot", true);
      MainClientActivity.this.startActivity(newIntent);
      Log.e("AdvMusic Client", "STARTED Redirect");
      finish();
      return;
    }

    Log.e("AdvMusic Client", "STARTED Second");

    Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 1);

    final WindowManager.LayoutParams lp = getWindow().getAttributes();
    lp.screenBrightness = 0.05f;// 100 / 100.0f;
    getWindow().setAttributes(lp);

    // PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
    // wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "ADVLOCK");
    // wl.acquire();

    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    final IntentFilter filterConfigFilesModified = new IntentFilter(
        "com.advmusicapp.configFilesModified");
    mConfigFilesModifiedReceiver = new ConfigFilesModifiedReceiver();
    this.registerReceiver(mConfigFilesModifiedReceiver, filterConfigFilesModified);

    final IntentFilter callFilter = new IntentFilter("android.intent.action.PHONE_STATE");
    // this.registerReceiver(phoneStateReceiver, callFilter);

    setVolumeControlStream(AudioManager.STREAM_MUSIC);

    setContentView(R.layout.main);

    songsList = (ListView) findViewById(R.id.songs);
    volume = (SeekBar) findViewById(R.id.volume);
    prev = (Button) findViewById(R.id.prev);
    playstop = (Button) findViewById(R.id.playstop);
    next = (Button) findViewById(R.id.next);

    initViews();

    mDropBoxHelper = DropBoxHelper.getInstance();
    mDropBoxHelper.setActivity(this);
    mDropBoxHelper.init();

    updateAlarms();

    instance = this;

    // scheduleReboot();

    SharedPreferences settings;
    settings = getSharedPreferences("AVD_PREF", Context.MODE_PRIVATE);

    if (afterBoot) {
      final String isPlaying = settings.getString("isPlaying", "");

      if (isPlaying.equals("true")) {
        playSong();
      }

      afterBoot = false;
    }

    final SharedPreferences.Editor editor = settings.edit();
    editor.putString("isPlaying", "false");
    editor.commit();

    // KeyguardManager mKeyGuardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
    // KeyguardLock mLock = mKeyGuardManager.newKeyguardLock("adv_client_app");
    // mLock.reenableKeyguard();
  }

  @Override
  protected void onPause() {
    super.onPause();
    // wl.release();
  }

  @Override
  protected void onResume() {
    super.onResume();
    // wl.acquire();
  }

  @Override
  public void onStart() {
    super.onStart();
    EasyTracker.getInstance().activityStart(this);
    FlurryAgent.onStartSession(this, "Y4NB4Z5T7QFSHPWFYCSY");
  }

  @Override
  public void onStop() {
    super.onStop();
    EasyTracker.getInstance().activityStop(this);
    FlurryAgent.onEndSession(this);
  }

  @Override
  protected void onDestroy() {
    stopSong(false);

    try {
      this.unregisterReceiver(mConfigFilesModifiedReceiver);
      // this.unregisterReceiver(phoneStateReceiver);
    } catch (final IllegalArgumentException e) {

    }

    if (mDropBoxHelper != null) {
      mDropBoxHelper.kill();
    }

    instance = null;

    super.onDestroy();
  }

  public void initViews() {
    initSongsAdapter();

    songsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(final AdapterView<?> adapterView, final View view, final int i,
          final long l) {
        currentPosition = i;
        playSong();
      }
    });

    prev.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        prevSong();
      }
    });

    next.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        nextSong(false);
      }
    });

    if (mp != null && mp.isPlaying()) {
      playstop.setText(getString(R.string.stop));
    } else {
      playstop.setText(getString(R.string.play));
    }

    playstop.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        if (songs == null || songs.size() == 0) {
          return;
        }

        if (mp != null && !mp.isPlaying()) {
          playSong();
        } else {
          stopSong(false);
        }
      }
    });

    audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

    final int volMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    volume.setMax(volMax);

    initSoundVolume();

    volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onStopTrackingTouch(final SeekBar arg0) {
      }

      @Override
      public void onStartTrackingTouch(final SeekBar arg0) {
      }

      @Override
      public void onProgressChanged(final SeekBar arg0, final int progress, final boolean arg2) {
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);

        final int volMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        final float volPercent = 100.0f / volMax;
        final int vol = Math.round(progress * volPercent);
        ClientHelper.clientData.setVolume(vol);
      }
    });

    playingNow = (TextView) findViewById(R.id.playingNow);

    if (getIntent().getExtras() != null) {
      afterBoot = getIntent().getExtras().getBoolean("afterBoot", false);

      if (getIntent().getExtras().getBoolean("crashed", false)) {
        getIntent().removeExtra("crashed");
        ClientHelper.clientData.setNowPlaying("Crashed!!!");
        ClientHelper.updateNowPlayingData(MainClientActivity.this);
      }

      if (afterBoot) {
        getIntent().removeExtra("afterBoot");
        return;
      }
    }

    initPassDialog();
  }

  private void initSoundVolume() {
    final int volMax = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    final float volPercent = 100.0f / volMax;
    final int clientVol = Math.round(ClientHelper.clientData.getVolume() / volPercent);

    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, clientVol, 0);
    volume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
  }

  private void initSongsAdapter() {
    songs = ClientHelper.clientData.getAudioFiles();

    songsAdapter = new CustomListAdapter(this, android.R.layout.simple_list_item_1, songs);

    songsList.setEmptyView(findViewById(R.id.empty_list_view));
    songsList.setAdapter(songsAdapter);
  }

  /**
   * Enter password dialog that appears after Done pressed in previous activity password is: sog
   */
  private void initPassDialog() {
    final EditText input = new EditText(this);
    final AlertDialog d = new AlertDialog.Builder(this).setView(input).setTitle("Locked")
        .setMessage("Enter password").setCancelable(false)
        .setPositiveButton(android.R.string.ok, new Dialog.OnClickListener() {
          @Override
          public void onClick(final DialogInterface d, final int which) {
          }
        }).create();

    d.setOnShowListener(new DialogInterface.OnShowListener() {

      @Override
      public void onShow(final DialogInterface dialog) {

        final Button b = d.getButton(AlertDialog.BUTTON_POSITIVE);
        b.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(final View view) {
            if (input.getText().toString().equals("sog")) {
              d.dismiss();
              // Execute empty SU command to force program ask for root on first start
              /*
               * try { final Process proc = Runtime.getRuntime().exec(new String[] { "su" }); }
               * catch (final Exception ex) { Log.i("AdvMusic Client", "Could not get su", ex);
               * 
               * FlurryAgent.onError("Catch", ex.getMessage(), "MainClientActivity - " + "176"); }
               */
            }
          }
        });
      }
    });

    d.show();
  }

  /**
   * Executing this after each PlaySong function will allow to force program to set zero volume if
   * someone will try to play songs at NIGHT time
   */
  private void checkSilentMode() {
    final Calendar morning = Calendar.getInstance();
    morning.set(Calendar.HOUR_OF_DAY, 7);
    morning.set(Calendar.MINUTE, 58);
    morning.set(Calendar.SECOND, 0);

    final Calendar evening = Calendar.getInstance();
    evening.set(Calendar.HOUR_OF_DAY, 21);
    evening.set(Calendar.MINUTE, 0);
    evening.set(Calendar.SECOND, 0);

    final Calendar now = Calendar.getInstance();

    if (now.after(evening) || now.before(morning)) {
      if (prevVolume == -1
          || (prevVolume != ClientHelper.clientData.getVolume() && ClientHelper.clientData
              .getVolume() > 0)) {
        prevVolume = ClientHelper.clientData.getVolume();
      }

      ClientHelper.clientData.setVolume(0);
      initSoundVolume();
    }

    else if (prevVolume != -1) {
      ClientHelper.clientData.setVolume(prevVolume);
      prevVolume = -1;
      initSoundVolume();
    }
  }

  /**
   * Create alarm for phone reboot
   * 
   * @param c
   *          - calendar time when to reboot
   * @param code
   *          - code for check
   */
  private void prepareAlarm(final Calendar c, final int code) {
    final long l = c.getTimeInMillis() - System.currentTimeMillis();
    long timeInMilis = c.getTimeInMillis();

    // If rebboting time is in past, we need
    // to increase it on 1 day
    if (l < 0) {
      timeInMilis += 86400000L;
    }

    final Intent intent = new Intent(this, AlarmRebootReceiver.class);
    intent.setAction(AlarmRebootReceiver.REBOOT_ALARM);
    final PendingIntent pendingIntent = PendingIntent.getBroadcast(this, code, intent,
        PendingIntent.FLAG_CANCEL_CURRENT);

    final AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    alarm.cancel(pendingIntent);
    alarm.setRepeating(AlarmManager.RTC_WAKEUP, timeInMilis, AlarmManager.INTERVAL_DAY,
        pendingIntent);
  }

  private void scheduleReboot() {
    final Calendar morning = Calendar.getInstance();
    morning.set(Calendar.HOUR_OF_DAY, 7);
    morning.set(Calendar.MINUTE, 0);
    morning.set(Calendar.SECOND, 0);

    // Calendar day = Calendar.getInstance();
    // day.set(Calendar.HOUR_OF_DAY, 16);
    // day.set(Calendar.MINUTE, 0);
    // day.set(Calendar.SECOND, 0);

    // Calendar evening = Calendar.getInstance();
    // evening.set(Calendar.HOUR_OF_DAY, 20);
    // evening.set(Calendar.MINUTE, 30);
    // evening.set(Calendar.SECOND, 0);

    prepareAlarm(morning, 676770);
    // prepareAlarm(day, 676771);
    // prepareAlarm(evening, 676772);
  }

  public synchronized void playSong() {
    isPlayingFromTimer = false;
    isPlaying = true;
    // PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
    // pm.reboot(null);

    checkSilentMode();

    songsList.invalidate();

    if (songs == null || songs.size() == 0) {
      return;
    }

    if (currentPosition < 0) {
      currentPosition = 0;
    }

    // ��� ������ ������ � ��������� �� ������� ���
    timerLogic();
    if (!isPlaying) {
      return;
    }

    // ================================================================================

    if (currentPosition >= songs.size()) {
      currentPosition = 0;
    }

    final String songName = songs.get(currentPosition);

    if (TextUtils.isEmpty(songName)) {
      errorPlayMusicCount++;
      nextSong(false);
      return;
    }

    currentlyPlaying = mDropBoxHelper.readAudioFile(songName);

    // FileDescriptor fd = mDropBoxHelper.readAudioFile(songName);

    // Go to next song if current could not be opened (from SD card)
    if (currentlyPlaying == null || currentlyPlaying.getFd() == null) {

      if (!ClientHelper.clientData.getFailedToPlayFiles().contains(songName)) {
        ClientHelper.clientData.getFailedToPlayFiles().add(songName);
      }

      errorPlayMusicCount++;
      nextSong(false);
      return;
    }

    if (ClientHelper.clientData.getDontPlayFlag()) {
      isPlayingFromTimer = false;
      isPlaying = false;

      if (playingNow != null) {
        playingNow.setText("Stopped forever");
      }

      if (mp != null && mp.isPlaying()) {
        mp.stop();
        playstop.setText(getString(R.string.play));
      }

      ClientHelper.clientData.setNowPlaying("Stopped forever");
      ClientHelper.updateNowPlayingData(MainClientActivity.this);

      return;
    }

    try {
      if (mp != null && mp.isPlaying()) {
        mp.stop();
        mp.reset();
        mp.release();
        mp = null;
      }

      mp = new MediaPlayer();
      mp.setDataSource(currentlyPlaying.getFd());
      mp.prepare();
      mp.start();

      mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

        @Override
        public void onCompletion(final MediaPlayer arg0) {

          if (mp != null) {
            if (mp.isPlaying()) {
              mp.stop();
            }
            mp.reset();
            mp.release();
            mp = null;
          }
          // closeDBFile();
          if (ClientHelper.clientData.getFailedToPlayFiles().contains(
              ClientHelper.clientData.getNowPlaying())) {
            ClientHelper.clientData.getFailedToPlayFiles().remove(
                ClientHelper.clientData.getNowPlaying());
          }

          nextSong(true);
        }
      });

      mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(final MediaPlayer arg0, final int arg1, final int arg2) {
          if (mp != null) {
            if (mp.isPlaying()) {
              mp.stop();
            }
            mp.reset();
            mp.release();
            mp = null;
          }
          // closeDBFile();
          errorPlayMusicCount++;

          if (!ClientHelper.clientData.getFailedToPlayFiles().contains(
              ClientHelper.clientData.getNowPlaying())) {
            ClientHelper.clientData.getFailedToPlayFiles().add(
                ClientHelper.clientData.getNowPlaying());
          }

          nextSong(false);

          return true;
        }
      });

      errorPlayMusicCount = 0;

      playingNow.setVisibility(View.VISIBLE);
      playingNow.setText("Playing: " + songName);
      playstop.setText(getString(R.string.stop));

      EasyTracker.getTracker().sendEvent("Client", "Play Song", songName, 0l);

      ClientHelper.clientData.setNowPlaying(songName);
      ClientHelper.updateNowPlayingData(MainClientActivity.this);

    } catch (final IOException e) {
      Log.e(getString(R.string.app_name), e.getMessage());
      FlurryAgent.onError("Catch", e.getMessage(), "MainClientActivity - " + "520");
      errorPlayMusicCount++;

      if (!ClientHelper.clientData.getFailedToPlayFiles().contains(songName)) {
        ClientHelper.clientData.getFailedToPlayFiles().add(songName);
      }

      nextSong(false);
    }
  }

  private void stopSongFromTimer() {
    isPlayingFromTimer = false;
    playingNow.setVisibility(View.GONE);
    playstop.setText(getString(R.string.play));
    ClientHelper.clientData.setNowPlaying("");
    ClientHelper.updateNowPlayingData(MainClientActivity.this);
  }

  private synchronized void playSongFromTimer(final String songName) {
    checkSilentMode();

    if (TextUtils.isEmpty(songName)) {
      stopSongFromTimer();
      return;
    }

    currentlyPlaying = mDropBoxHelper.readAudioFile(songName);

    // Go to next song if current could not be opened (from SD card)
    if (currentlyPlaying == null || currentlyPlaying.getFd() == null) {
      if (!ClientHelper.clientData.getFailedToPlayFiles().contains(songName)) {
        ClientHelper.clientData.getFailedToPlayFiles().add(songName);
      }

      stopSongFromTimer();
      return;
    }

    if (ClientHelper.clientData.getDontPlayFlag()) {
      isPlayingFromTimer = false;

      if (playingNow != null) {
        playingNow.setText("Stopped forever");
      }

      if (mp != null && mp.isPlaying()) {
        mp.stop();
        playstop.setText(getString(R.string.play));
      }

      ClientHelper.clientData.setNowPlaying("Stopped forever");
      ClientHelper.updateNowPlayingData(MainClientActivity.this);

      return;
    }

    try {
      if (mp != null && mp.isPlaying()) {
        return;
      }

      mp = new MediaPlayer();
      mp.setDataSource(currentlyPlaying.getFd());
      mp.prepare();
      mp.start();

      mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(final MediaPlayer arg0) {
          if (mp != null) {
            if (mp.isPlaying()) {
              mp.stop();
            }
            mp.reset();
            mp.release();
            mp = null;
          }

          if (ClientHelper.clientData.getFailedToPlayFiles().contains(
              ClientHelper.clientData.getNowPlaying())) {
            ClientHelper.clientData.getFailedToPlayFiles().remove(
                ClientHelper.clientData.getNowPlaying());
          }

          stopSongFromTimer();
          timerLogicIfStopped();
          return;
        }
      });

      mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(final MediaPlayer arg0, final int arg1, final int arg2) {
          if (mp != null) {
            if (mp.isPlaying()) {
              mp.stop();
            }
            mp.reset();
            mp.release();
            mp = null;
          }

          if (!ClientHelper.clientData.getFailedToPlayFiles().contains(
              ClientHelper.clientData.getNowPlaying())) {
            ClientHelper.clientData.getFailedToPlayFiles().add(
                ClientHelper.clientData.getNowPlaying());
          }

          stopSongFromTimer();
          return true;
        }
      });

      playingNow.setVisibility(View.VISIBLE);
      playingNow.setText("Playing: " + songName);
      playstop.setText(getString(R.string.stop));

      ClientHelper.clientData.setNowPlaying(songName);
      ClientHelper.updateNowPlayingData(MainClientActivity.this);
    }

    catch (final IOException e) {
      if (!ClientHelper.clientData.getFailedToPlayFiles().contains(songName)) {
        ClientHelper.clientData.getFailedToPlayFiles().add(songName);
      }

      stopSongFromTimer();
    }
  }

  private void timerLogicIfStopped() {
    if (isPlaying || isPlayingFromTimer) {
      return;
    }

    isPlayingFromTimer = true;

    final HashMap<String, ArrayList<Long>> aFtoP = ClientHelper.clientData
        .getAudioFilesTimeToPlay();

    if (aFtoP != null) {
      for (final Entry<String, ArrayList<Long>> entry : aFtoP.entrySet()) {
        for (int i = 0; i < entry.getValue().size(); i++) {
          final Calendar c = Calendar.getInstance();
          final long l = c.getTimeInMillis();

          if (l >= entry.getValue().get(i)) {
            final long time = entry.getValue().get(i) + 86400000L;
            entry.getValue().set(i, time);

            if (songs.indexOf(entry.getKey()) >= 0) {
              playSongFromTimer(entry.getKey());
              return;
            }
          }
        }
      }
    }

    isPlayingFromTimer = false;
  }

  private void timerLogic() {
    final HashMap<String, ArrayList<Long>> aFtoP = ClientHelper.clientData
        .getAudioFilesTimeToPlay();

    if (aFtoP != null) {
      for (final Entry<String, ArrayList<Long>> entry : aFtoP.entrySet()) {
        for (int i = 0; i < entry.getValue().size(); i++) {
          final Calendar c = Calendar.getInstance();
          final long l = c.getTimeInMillis();

          if (l >= entry.getValue().get(i)) {
            final long time = entry.getValue().get(i) + 86400000L;
            entry.getValue().set(i, time);

            if (songs.indexOf(entry.getKey()) >= 0) {
              storeCountSong = currentPosition;
              currentPosition = songs.indexOf(entry.getKey());
              return;
            }
          }
        }
      }
    }

    // Dont play timer song even after click, or after startPlay command from server
    final ArrayList<Long> arrayList = ClientHelper.clientData.getAudioFilesTimeToPlay().get(
        songs.get(currentPosition));

    if (arrayList != null && !arrayList.isEmpty()) {
      errorPlayMusicCount++;
      nextSong(false);
    }
  }

  public void stopSong(final boolean dontSend) {
    isPlayingFromTimer = false;
    isPlaying = false;
    // closeDBFile();

    if (playingNow != null) {
      playingNow.setVisibility(View.GONE);
    }

    if (mp != null && mp.isPlaying()) {
      mp.stop();
      playstop.setText(getString(R.string.play));

      EasyTracker.getTracker().sendEvent("Client", "Stop Song", "", 0l);

      ClientHelper.clientData.setNowPlaying("");
      // if (!dontSend) {
      ClientHelper.updateNowPlayingData(MainClientActivity.this);
      // ClientHelper.clientData.getFailedToPlayFiles().clear();
      // }
    }
  }

  private void prevSong() {
    if (songs == null || songs.size() == 0) {
      return;
    }

    if (currentPosition < 0) {
      currentPosition = 0;
    }

    if (--currentPosition < 0) {
      currentPosition = songs.size() - 1;

      if (errorPlayMusicCount >= songs.size()) {
        errorPlayMusicCount = 0;
        stopSong(false);
        return;
      }
    }

    EasyTracker.getTracker().sendEvent("Client", "Prev Song", "", 0l);

    final ArrayList<Long> arrayList = ClientHelper.clientData.getAudioFilesTimeToPlay().get(
        songs.get(currentPosition));

    if (arrayList != null && !arrayList.isEmpty()) {
      errorPlayMusicCount++;
      prevSong();
      return;
    } else {
      playSong();
      return;
    }
  }

  private void nextSong(final boolean playWithDelay) {
    if (currentPosition < 0) {
      currentPosition = 0;
    }

    if (songs == null || songs.size() == 0) {
      return;
    }

    if (++currentPosition >= songs.size()) {
      currentPosition = 0;

      if (errorPlayMusicCount >= songs.size()) {
        errorPlayMusicCount = 0;
        stopSong(false);
        return;
      }
    }

    EasyTracker.getTracker().sendEvent("Client", "Next Song", "", 0l);

    final ArrayList<Long> arrayList = ClientHelper.clientData.getAudioFilesTimeToPlay().get(
        songs.get(currentPosition));

    if (arrayList != null && !arrayList.isEmpty()) {
      errorPlayMusicCount++;
      nextSong(false);
    }

    else {
      if (storeCountSong != -1) {
        currentPosition = storeCountSong;
        storeCountSong = -1;
      }

      if (playWithDelay && Constants.CURR_BUILD_TYPE != BuildType.SHTENDER) {
        DropBoxHelper.mHandler.postDelayed(new Runnable() {
          @Override
          public void run() {
            playSong();
          }
        }, Constants.NEXT_SONG_DELAY * 1000);
      }

      else {
        playSong();
      }
    }
  }

  /**
   * Update auto playSong and stopSong alarms after activity started or config file changed on
   * server
   */
  private void updateAlarms() {

    EasyTracker.getTracker().sendEvent("Client", "Update Alarms", "", 0l);

    updateStartAlarm();
    updateStopAlarm();
  }

  private void updateStartAlarm() {
    if (TextUtils.isEmpty(ClientHelper.clientData.getStartTime())) {
      ClientHelper.clientData.setStartTime("7:30");
    }
    updateAlarm(AlarmStartReceiver.class, 676761, AlarmStartReceiver.START_ALARM,
        ClientHelper.clientData.getStartTime());
  }

  private void updateStopAlarm() {
    if (TextUtils.isEmpty(ClientHelper.clientData.getStopTime())) {
      ClientHelper.clientData.setStopTime("21:00");
    }
    updateAlarm(AlarmStopReceiver.class, 676762, AlarmStopReceiver.STOP_ALARM,
        ClientHelper.clientData.getStopTime());
  }

  /**
   * Create alarm for playSong/stopSong
   */
  private void updateAlarm(final Class<?> clazz, final int index, final String action,
      final String time) {
    final String[] parsedTime = time.split(":");
    final int hour = Integer.valueOf(parsedTime[0]);
    final int minute = Integer.valueOf(parsedTime[1]);

    final Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, hour);

    if (hour <= 12) {
      calendar.set(Calendar.HOUR, hour);
      calendar.set(Calendar.AM_PM, 0);
    } else {
      calendar.set(Calendar.HOUR, hour - 12);
      calendar.set(Calendar.AM_PM, 1);
    }

    calendar.set(Calendar.MINUTE, minute);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    final Intent intent = new Intent(this, clazz);
    intent.setAction(action);

    // long l = calendar.getTimeInMillis() - System.currentTimeMillis();
    long timeInMilis = calendar.getTimeInMillis();

    // inc alarm time on 1day
    // if it is in past
    while (Calendar.getInstance().getTimeInMillis() >= timeInMilis) {
      timeInMilis += 86400000L;
      // l = timeInMilis - System.currentTimeMillis();
    }

    // intent.putExtra("diff", l);

    EasyTracker.getTracker().sendEvent("Client", "Update Alarms: " + clazz.getName(), time,
        (long) 0);

    final PendingIntent pendingIntent = PendingIntent.getBroadcast(this, index, intent,
        PendingIntent.FLAG_CANCEL_CURRENT);

    final AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    alarm.cancel(pendingIntent);
    alarm.setRepeating(AlarmManager.RTC_WAKEUP, timeInMilis, AlarmManager.INTERVAL_DAY,
        pendingIntent);
  }

  /**
   * Get new config file form web server in separate thread
   * 
   */
  class ConfigFilesModifiedTask extends AsyncTask<List<String>, Void, Void> {
    @Override
    protected Void doInBackground(final List<String>... params) {
      ConfigFilesModified();

      DropBoxHelper.mHandler.post(new Runnable() {
        @Override
        public void run() {
          DropBoxHelper.showToastMsg("Updated");

          if (songsAdapter != null) {
            songsAdapter.notifyDataSetChanged();
          }
        }
      });

      return null;
    }

    @Override
    protected void onPostExecute(final Void v) {
      DropBoxHelper.mHandler.post(new Runnable() {
        @Override
        public void run() {
          timerLogicIfStopped();
        }
      });
    }
  }

  /**
   * Try to get new client data from web server. Compare it with current data, then parse and save
   * if received data has new info
   * 
   */
  private synchronized void ConfigFilesModified() {
    final JSONObject jsonObject = mDropBoxHelper.readConfigFile(ClientHelper.clientData.getName(),
        true);

    if (jsonObject != null && !ClientHelper.clientData.compareJSONAndModel(jsonObject)) {
      ClientData.fromJSON(jsonObject);

      DropBoxHelper.mHandler.post(new Runnable() {
        @Override
        public void run() {
          if (!songs.equals(ClientHelper.clientData.getAudioFiles())) {
            stopSong(true);
            currentPosition = 0;
            initSongsAdapter();
          }

          initSoundVolume();
          updateAlarms();

          final int songStatus = ClientHelper.clientData.getSongStatus();
          switch (songStatus) {
          case 0:
            stopSong(false);
            break;
          case 1:
            playSong();
            break;
          }
        }
      });

      // mDropBoxHelper.showToastMsg(getString(R.string.info_received) + ": " + "Server");
    }
  }

  /**
   * Getting new config data check event from DropBoxHelper
   * 
   */
  private class ConfigFilesModifiedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context arg0, final Intent arg1) {
      new ConfigFilesModifiedTask().execute();
    }
  }

  @Override
  public boolean onKeyDown(final int keyCode, final KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN || keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
      volume.setProgress(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));
      return false;
    }

    /*
     * else if (keyCode == KeyEvent.KEYCODE_POWER) { Intent intent = new Intent(Intent.ACTION_MAIN);
     * intent.addCategory(Intent.CATEGORY_HOME); intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     * startActivity(intent); return false; }
     */

    /*
     * else if (keyCode == KeyEvent.KEYCODE_BACK) { Intent intent = new Intent(Intent.ACTION_MAIN);
     * intent.addCategory(Intent.CATEGORY_HOME); intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     * startActivity(intent); return false; }
     */

    else {
      return super.onKeyDown(keyCode, event);
    }
  }

  /**
   * Pausing and resuming music when phone ringing
   * 
   */
  private final BroadcastReceiver phoneStateReceiver = new BroadcastReceiver() {

    @Override
    public void onReceive(final Context context, final Intent intent) {
      if (!intent.getAction().equals("android.intent.action.PHONE_STATE")) {
        return;
      }
      final String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

      if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)
          || state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
        Log.d("DEBUG", "OFFHOOK");

        if (mp != null && audioInPause) {
          audioInPause = false;
          mp.seekTo(songCurrentPosition);
          mp.start();
        }

      }
      if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
        Log.d("DEBUG", "RINGING");

        if (mp != null && mp.isPlaying()) {
          audioInPause = true;
          mp.pause();
          songCurrentPosition = mp.getCurrentPosition();
        }

        // Intent answer = new Intent(Intent.ACTION_MEDIA_BUTTON);
        // answer.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP,
        // KeyEvent.KEYCODE_HEADSETHOOK));
        // sendOrderedBroadcast(answer, null);

        // Call a service, since this could take a few seconds
        context.startService(new Intent(context, AutoAnswerIntentService.class));

      }

    }
  };

  private class CustomListAdapter extends ArrayAdapter<String> {

    private final Context mContext;
    private final int id;
    private final List<String> items;

    public CustomListAdapter(final Context context, final int textViewResourceId,
        final List<String> list) {
      super(context, textViewResourceId, list);
      mContext = context;
      id = textViewResourceId;
      items = list;
    }

    @Override
    public View getView(final int position, final View v, final ViewGroup parent) {
      View mView = v;
      if (mView == null) {
        final LayoutInflater vi = (LayoutInflater) mContext
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = vi.inflate(id, null);
      }

      final TextView text = (TextView) mView.findViewById(android.R.id.text1);

      final ArrayList<Long> timers = ClientHelper.clientData.getAudioFilesTimeToPlay().get(
          items.get(position));

      if (items.get(position) != null) {
        if (timers != null && !timers.isEmpty()) {

          long tmpTime = timers.get(0);
          for (int i = 0; i < timers.size(); i++) {
            if (timers.get(i) < tmpTime) {
              tmpTime = timers.get(i);
            }
          }

          final Calendar c = Calendar.getInstance();
          final Calendar cc = Calendar.getInstance();
          c.setTimeInMillis(tmpTime);

          if (c.get(Calendar.DAY_OF_MONTH) == cc.get(Calendar.DAY_OF_MONTH)) {
            text.setText(items.get(position) + " " + timers.size() + "�. - "
                + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE));
          } else {
            text.setText(items.get(position) + " " + timers.size() + "�. - "
                + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + "+++");
          }

          text.setBackgroundColor(Color.RED);
          final int color = Color.argb(200, 255, 64, 64);
          text.setBackgroundColor(color);
        } else {
          text.setText(items.get(position));
          text.setBackgroundColor(Color.TRANSPARENT);
        }

      }

      return mView;
    }
  }
}
