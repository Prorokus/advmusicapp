package com.advmusicapp;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.advmusicapp.client.R;
import com.advmusicapp.helpers.ClientHelper;
import com.advmusicapp.helpers.ConnectionHelper;
import com.advmusicapp.helpers.DropBoxHelper;
import com.advmusicapp.objects.ClientData;
import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 11.02.13 Time: 0:48 To change this template use File
 * | Settings | File Templates.
 */
public class FirstLoadActivity extends Activity {

  private EditText city;
  private EditText name;
  private Button done;

  private DropBoxHelper mDropBoxHelper;

  // private PowerManager.WakeLock wl;

  private FilesSynchedOnStartReceiver mFilesSynchedOnStartReceiver;

  private ProgressDialog mProgressDialog;

  private boolean afterBoot = false;

  int bv = Build.VERSION.SDK_INT;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    Log.e("AdvMusic Client", "STARTED First");
    super.onCreate(savedInstanceState);

    // Ensure one instance of your main activity
    final Intent intent = getIntent();
    final String intentAction = intent.getAction();

    final File f = new File(Environment.getExternalStorageDirectory() + "/AdvMusic/audio");
    if (!f.exists()) {
      f.mkdirs();
    }

    if (!isTaskRoot() && intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null
        && intentAction.equals(Intent.ACTION_MAIN)) {
      finish();
    } else {
      // toggleWiFi(true);
      // turnOnDataConnection(true, FirstLoadActivity.this);

      mProgressDialog = new ProgressDialog(FirstLoadActivity.this);
      mProgressDialog.setCancelable(false);

      Crashlytics.start(this);
      final KeyguardManager mKeyGuardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
      final KeyguardLock mLock = mKeyGuardManager.newKeyguardLock("adv_client_app");
      mLock.disableKeyguard();

      Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS, 1);

      final WindowManager.LayoutParams lp = getWindow().getAttributes();
      lp.screenBrightness = 0.05f;// 100 / 100.0f;
      getWindow().setAttributes(lp);

      // PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
      // wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "ADVLOCK");
      // wl.acquire();

      getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

      android.provider.Settings.System.putInt(getContentResolver(),
          android.provider.Settings.System.WIFI_SLEEP_POLICY,
          android.provider.Settings.System.WIFI_SLEEP_POLICY_NEVER);

      afterBoot = false;

      setContentView(R.layout.firstload);

      city = (EditText) findViewById(R.id.city);
      name = (EditText) findViewById(R.id.name);
      done = (Button) findViewById(R.id.done);

      mDropBoxHelper = DropBoxHelper.getInstance();
      mDropBoxHelper.setActivity(FirstLoadActivity.this);

      ClientHelper.clientData = new ClientData(FirstLoadActivity.this);

      initViews();

      readData();
    }
  }

  private void toggleWiFi(final boolean turn) {
    final WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE);
    wifiManager.setWifiEnabled(turn);
  }

  /**
   * Enable/Disable data transfering (edge, 3g, 4g internet)
   * 
   */
  boolean turnOnDataConnection(final boolean ON, final Context context) {

    try {
      if (bv == Build.VERSION_CODES.FROYO)

      {
        Method dataConnSwitchmethod;
        Class<?> telephonyManagerClass;
        Object ITelephonyStub;
        Class<?> ITelephonyClass;

        final TelephonyManager telephonyManager = (TelephonyManager) context
            .getSystemService(Context.TELEPHONY_SERVICE);

        telephonyManagerClass = Class.forName(telephonyManager.getClass().getName());
        final Method getITelephonyMethod = telephonyManagerClass.getDeclaredMethod("getITelephony");
        getITelephonyMethod.setAccessible(true);
        ITelephonyStub = getITelephonyMethod.invoke(telephonyManager);
        ITelephonyClass = Class.forName(ITelephonyStub.getClass().getName());

        if (ON) {
          dataConnSwitchmethod = ITelephonyClass.getDeclaredMethod("enableDataConnectivity");
        } else {
          dataConnSwitchmethod = ITelephonyClass.getDeclaredMethod("disableDataConnectivity");
        }
        dataConnSwitchmethod.setAccessible(true);
        dataConnSwitchmethod.invoke(ITelephonyStub);

      } else {
        // log.i("App running on Ginger bread+");
        final ConnectivityManager conman = (ConnectivityManager) context
            .getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class<?> conmanClass = Class.forName(conman.getClass().getName());
        final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
        iConnectivityManagerField.setAccessible(true);
        final Object iConnectivityManager = iConnectivityManagerField.get(conman);
        final Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass()
            .getName());
        final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod(
            "setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);
        setMobileDataEnabledMethod.invoke(iConnectivityManager, ON);
      }

      return true;
    }

    catch (final Exception e) {
      Log.e("ADV_CLIENT", "error turning on/off data");
      FlurryAgent.onError("Catch", e.getMessage(), "FirstLoadActivity - " + "168");
      return false;
    }

  }

  @Override
  public void onStart() {
    super.onStart();
    EasyTracker.getInstance().activityStart(FirstLoadActivity.this);
    FlurryAgent.onStartSession(this, "Y4NB4Z5T7QFSHPWFYCSY");
  }

  @Override
  public void onStop() {
    super.onStop();
    EasyTracker.getInstance().activityStop(FirstLoadActivity.this);
    FlurryAgent.onEndSession(this);
  }

  @Override
  protected void onDestroy() {
    if (mProgressDialog != null) {
      mProgressDialog.dismiss();
    }

    super.onDestroy();
  }

  /**
   * Starting main activity after Done button pressed
   * 
   */
  public class StartClientTask extends AsyncTask<String, Void, Integer> {

    @Override
    protected void onPreExecute() {
      mProgressDialog.setMessage("Please wait...");
      mProgressDialog.show();
    }

    @Override
    protected Integer doInBackground(final String... strings) {
      // Read client data
      JSONObject jsonObject = null;

      jsonObject = mDropBoxHelper.readConfigFile(strings[1], false);

      if (jsonObject != null) {
        // Parse client JSON from server
        ClientData.fromJSON(jsonObject);

        DropBoxHelper.mHandler.post(new Runnable() {
          @Override
          public void run() {
            city.setText(ClientHelper.clientData.getCity());
            name.setText(ClientHelper.clientData.getName());
          }
        });
      }

      saveData();

      startMainActivity(strings[0], strings[1]);

      return null;
    }

    @Override
    protected void onPostExecute(final Integer result) {
      if (mProgressDialog.isShowing()) {
        mProgressDialog.hide();
      }
    }
  }

  // Root needed

  private void reboot() {
    try {
      final Process proc = Runtime.getRuntime().exec(new String[] { "su", "-c", "reboot" });
      proc.waitFor();
    } catch (final Exception ex) {
      Log.i("AdvMusic Client", "Could not reboot", ex);
      FlurryAgent.onError("Catch", ex.getMessage(), "FirstLoadActivity - " + "255");
    }
  }

  /**
   * Auto starting client after phone booted
   * 
   */
  public class StartClientAfterBootTask extends AsyncTask<String, Void, Integer> {
    @Override
    protected void onPreExecute() {
      mProgressDialog.setMessage("Please wait...");
      mProgressDialog.show();
    }

    @Override
    protected Integer doInBackground(final String... strings) {
      JSONObject jsonObject = null;

      int getDataFailCounter = 0;
      int emptySongsCounter = 0;

      // -----------------------CONNECTING---------------------------------

      DropBoxHelper.mHandler.post(new Runnable() {
        @Override
        public void run() {
          mProgressDialog.setMessage("��� ��������� �����");
        }
      });

      // wait for 3g connection appear
      while (!ConnectionHelper.isNetworkAvailable(FirstLoadActivity.this)) {
        // reboot after 5 fails

        // if (getDataFailCounter >= 5) {
        FlurryAgent.onError("Reboot", "no connection", "FirstLoadActivity - " + "271");

        DropBoxHelper.mHandler.post(new Runnable() {

          @Override
          public void run() {
            Toast.makeText(FirstLoadActivity.this,
                "����� ����, ����� 5 �������, ������ ���������������", Toast.LENGTH_LONG).show();
          }
        });

        // reboot();
        // }

        // toggleWiFi(true);
        // turnOnDataConnection(true, FirstLoadActivity.this);

        // try to sleep for 20 seconds and check connection again
        try {
          DropBoxHelper.mHandler.post(new Runnable() {
            @Override
            public void run() {
              Toast
                  .makeText(FirstLoadActivity.this, "����� ����, ��� 20 ������", Toast.LENGTH_LONG)
                  .show();
            }
          });

          Thread.sleep(20000);
          getDataFailCounter++;
        }

        catch (final InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
          FlurryAgent.onError("Catch", e.getMessage(), "FirstLoadActivity - " + "259");
        }
      }

      // -----------------------RECEIVING DATA---------------------------------

      DropBoxHelper.mHandler.post(new Runnable() {
        @Override
        public void run() {
          mProgressDialog.setMessage("����� �����������, ��������� ������");
        }
      });

      // Try to receive data from server
      while (jsonObject == null) {
        jsonObject = mDropBoxHelper.readConfigFile(strings[1], false);

        // Web server is offline, sleep for 60 seconds
        if (jsonObject == null) {
          DropBoxHelper.mHandler.post(new Runnable() {
            @Override
            public void run() {
              Toast.makeText(FirstLoadActivity.this, "���� �� ��������, ��� 60 ������",
                  Toast.LENGTH_LONG).show();
            }
          });

          try {
            Thread.sleep(60000);
          }

          catch (final InterruptedException e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "FirstLoadActivity - " + "274");
          }
        }

        else {
          try {
            // Sometimes web server accidently returns empty song list
            // after boot for 1 or 2 times.
            // Here we try to understand: is songs list really empty? or it was accident
            if (jsonObject.getString("audioFiles").equals("[]")) {
              if (emptySongsCounter >= 3) {
                DropBoxHelper.mHandler.post(new Runnable() {
                  @Override
                  public void run() {
                    Toast.makeText(FirstLoadActivity.this,
                        "����� 3� ������� ������ ����� ������, �������� ��� � ����?",
                        Toast.LENGTH_LONG).show();
                  }
                });
              }

              else {
                DropBoxHelper.mHandler.post(new Runnable() {
                  @Override
                  public void run() {
                    Toast.makeText(FirstLoadActivity.this,
                        "������ ������ ������ �����, �������� ��� ������, ��� 20 ������",
                        Toast.LENGTH_LONG).show();
                  }
                });

                emptySongsCounter++;
                jsonObject = null;
                Thread.sleep(20000);
              }
            }
          }

          catch (final InterruptedException e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "FirstLoadActivity - " + "289");
          }

          catch (final JSONException e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "FirstLoadActivity - " + "293");
          }
        }
      }

      // -----------------------DATA RECEIVED---------------------------------

      // Parce JSON response
      if (jsonObject != null) {
        ClientData.fromJSON(jsonObject);

        DropBoxHelper.mHandler.post(new Runnable() {
          @Override
          public void run() {
            city.setText(ClientHelper.clientData.getCity());
            name.setText(ClientHelper.clientData.getName());
          }
        });
      }

      saveData();

      // Log.e("AdvMusic Client", ClientHelper.clientData.getAudioFiles().toString());

      startMainActivity(strings[0], strings[1]);

      return null;
    }

    @Override
    protected void onPostExecute(final Integer result) {
      if (mProgressDialog.isShowing()) {
        mProgressDialog.hide();
      }
    }
  }

  private void initViews() {
    done.setEnabled(true);
    done.setText("Done");

    done.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {

        final String cityStr = city.getText().toString();
        final String nameStr = name.getText().toString();

        if (TextUtils.isEmpty(cityStr) || TextUtils.isEmpty(nameStr)) {
          return;
        }

        new StartClientTask().execute(cityStr, nameStr);
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();

    final IntentFilter filesSynchedOnStart = new IntentFilter("com.advmusicapp.filesSynchedOnStart");
    mFilesSynchedOnStartReceiver = new FilesSynchedOnStartReceiver();
    this.registerReceiver(mFilesSynchedOnStartReceiver, filesSynchedOnStart);

    // wl.acquire();
  }

  @Override
  public void onPause() {
    super.onPause();
    unregisterReceiver(mFilesSynchedOnStartReceiver);

    // wl.release();
  }

  private class FilesSynchedOnStartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context arg0, final Intent arg1) {
      /*
       * JSONObject jsonObject = mDropBoxHelper.readConfigFile(ClientHelper.clientData.getUuid());
       * 
       * if(jsonObject != null){ ClientHelper.clientData = ClientData.fromJSON(jsonObject);
       * city.setText(ClientHelper.clientData.getCity());
       * name.setText(ClientHelper.clientData.getName()); }
       */
    }
  }

  /**
   * Save data to Shared Prefs
   * 
   */
  private void saveData() {
    SharedPreferences settings;
    settings = getSharedPreferences("AVD_PREF", Context.MODE_PRIVATE);

    final SharedPreferences.Editor editor = settings.edit();

    final String cityStr = city.getText().toString();
    final String nameStr = name.getText().toString();

    editor.putString("city", cityStr);
    editor.putString("name", nameStr);

    editor.commit();
  }

  /**
   * Load data from Shared Prefs
   * 
   */
  private void readData() {
    SharedPreferences settings;
    settings = getSharedPreferences("AVD_PREF", Context.MODE_PRIVATE);

    final String cityStr = settings.getString("city", null);
    final String nameStr = settings.getString("name", null);

    if (TextUtils.isEmpty(cityStr) || TextUtils.isEmpty(nameStr)) {
      return;
    }

    city.setText(cityStr);
    name.setText(nameStr);

    // Auto starting main activity after phone boot
    if (getIntent().getExtras() != null) {
      afterBoot = getIntent().getExtras().getBoolean("afterBoot", false);

      if (afterBoot) {
        getIntent().removeExtra("afterBoot");

        done.setEnabled(false);
        done.setText("Connecting!!!");

        final KeyguardManager mKeyGuardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        final KeyguardLock mLock = mKeyGuardManager.newKeyguardLock("adv_client_app");
        mLock.reenableKeyguard();

        new StartClientAfterBootTask().execute(cityStr, nameStr);
      }
    }
  }

  /**
   * Start main activity with client playlist
   * 
   */
  private void startMainActivity(final String cityStr, final String nameStr) {
    ClientHelper.updateMainData(cityStr, nameStr);

    final Intent intent = new Intent(FirstLoadActivity.this, MainClientActivity.class);

    if (afterBoot) {
      intent.putExtra("afterBoot", true);

      if (getIntent().getExtras() != null && getIntent().getExtras().getBoolean("crashed", false)) {
        intent.putExtra("crashed", true);
      }
    }

    DropBoxHelper.mHandler.post(new Runnable() {
      @Override
      public void run() {
        Toast.makeText(FirstLoadActivity.this, "START!!!!!!!!!!!!!!!!!!!", Toast.LENGTH_LONG)
            .show();
      }
    });

    startActivity(intent);
    finish();
  }
}
