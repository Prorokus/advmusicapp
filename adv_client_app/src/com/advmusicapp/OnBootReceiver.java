package com.advmusicapp;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

	public class OnBootReceiver extends BroadcastReceiver
	{
	    @Override
	    public void onReceive(Context context, Intent intent)
	    {
	        Log.d("AdvMusicClient","booot");
	        Toast.makeText(context, "Loaded", Toast.LENGTH_LONG).show();
	        
  	    	//KeyguardManager mKeyGuardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
  	    	//KeyguardLock mLock = mKeyGuardManager.newKeyguardLock("adv_client_app");
  	    	//mLock.disableKeyguard();	 
	    	
	        Intent newIntent = new Intent(context, FirstLoadActivity.class);
	        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        newIntent.putExtra("afterBoot", true);
	        context.startActivity(newIntent);
	    }
	}	
