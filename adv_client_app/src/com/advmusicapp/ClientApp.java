package com.advmusicapp;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;

import com.advmusicapp.helpers.DropBoxHelper;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 12.02.13 Time: 0:36 To change this template use File
 * | Settings | File Templates.
 */
public class ClientApp extends Application {

  private DropBoxHelper dbHelper;

  private static Application instance;

  @Override
  public void onCreate() {
    Constants.initUrlStrings();

    Thread.setDefaultUncaughtExceptionHandler(new TryMe());

    super.onCreate();

    dbHelper = new DropBoxHelper(getApplicationContext());

    instance = this;
  }

  public static Application getInstance() {
    return instance;
  }

  public class TryMe implements Thread.UncaughtExceptionHandler {

    Thread.UncaughtExceptionHandler oldHandler;

    public TryMe() {
      oldHandler = Thread.getDefaultUncaughtExceptionHandler(); // �������� ����� �������������
                                                                // ����������
    }

    @Override
    public void uncaughtException(final Thread thread, final Throwable throwable) {
      final Intent newIntent = new Intent(instance.getApplicationContext(), FirstLoadActivity.class);
      newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      newIntent.putExtra("afterBoot", true);
      newIntent.putExtra("crashed", true);
      final PendingIntent pI = PendingIntent.getActivity(instance.getApplicationContext(), 0,
          newIntent, newIntent.getFlags());

      final AlarmManager mgr = (AlarmManager) getSystemService(instance.getApplicationContext().ALARM_SERVICE);
      mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, pI);
      System.exit(2);

      if (oldHandler != null) {
        oldHandler.uncaughtException(thread, throwable); // ...������� ���
      }
    }
  }
}
