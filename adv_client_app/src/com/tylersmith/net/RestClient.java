/*
 * Copyright (C) 2011 Tyler Smith.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tylersmith.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import android.content.Context;

import com.flurry.android.FlurryAgent;

public class RestClient {

  private boolean authentication;
  private final ArrayList<NameValuePair> headers;

  private String jsonBody;
  private String message;

  private final ArrayList<NameValuePair> params;
  private String response;
  private int responseCode;

  private final String url;

  // HTTP Basic Authentication
  private String username;
  private String password;

  protected Context context;

  public RestClient(final String url) {
    this.url = url;
    params = new ArrayList<NameValuePair>();
    headers = new ArrayList<NameValuePair>();
  }

  // Be warned that this is sent in clear text, don't use basic auth unless you have to.
  public void addBasicAuthentication(final String user, final String pass) {
    authentication = true;
    username = user;
    password = pass;
  }

  public void addHeader(final String name, final String value) {
    headers.add(new BasicNameValuePair(name, value));
  }

  public void addParam(final String name, final String value) {
    params.add(new BasicNameValuePair(name, value));
  }

  public void execute(final RequestMethod method) throws Exception {
    switch (method) {
    case GET: {
      HttpGet request = new HttpGet(url + addGetParams());
      request = (HttpGet) addHeaderParams(request);
      executeRequest(request, url);
      break;
    }
    case POST: {
      HttpPost request = new HttpPost(url);
      request = (HttpPost) addHeaderParams(request);
      request = (HttpPost) addBodyParams(request);
      executeRequest(request, url);
      break;
    }
    case PUT: {
      HttpPut request = new HttpPut(url);
      request = (HttpPut) addHeaderParams(request);
      request = (HttpPut) addBodyParams(request);
      executeRequest(request, url);
      break;
    }
    case DELETE: {
      HttpDelete request = new HttpDelete(url);
      request = (HttpDelete) addHeaderParams(request);
      executeRequest(request, url);
    }
    }
  }

  private HttpUriRequest addHeaderParams(final HttpUriRequest request) throws Exception {
    for (final NameValuePair h : headers) {
      request.addHeader(h.getName(), h.getValue());
    }

    if (authentication) {

      final UsernamePasswordCredentials creds = new UsernamePasswordCredentials(username, password);
      request.addHeader(new BasicScheme().authenticate(creds, request));
    }

    return request;
  }

  private HttpUriRequest addBodyParams(final HttpUriRequest request) throws Exception {
    if (jsonBody != null) {
      request.addHeader("Content-Type", "application/json");
      if (request instanceof HttpPost) {
        ((HttpPost) request).setEntity(new StringEntity(jsonBody, "UTF-8"));
      } else if (request instanceof HttpPut) {
        ((HttpPut) request).setEntity(new StringEntity(jsonBody, "UTF-8"));
      }

    } else if (!params.isEmpty()) {
      if (request instanceof HttpPost) {
        ((HttpPost) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
      } else if (request instanceof HttpPut) {
        ((HttpPut) request).setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
      }
    }
    return request;
  }

  private String addGetParams() throws Exception {
    final StringBuffer combinedParams = new StringBuffer();
    if (!params.isEmpty()) {
      combinedParams.append("?");
      for (final NameValuePair p : params) {
        combinedParams.append((combinedParams.length() > 1 ? "&" : "") + p.getName() + "="
            + URLEncoder.encode(p.getValue(), "UTF-8"));
      }
    }
    return combinedParams.toString();
  }

  public String getErrorMessage() {
    return message;
  }

  public String getResponse() {
    return response;
  }

  public int getResponseCode() {
    return responseCode;
  }

  public void setContext(final Context ctx) {
    context = ctx;
  }

  public void setJSONString(final String data) {
    jsonBody = data;
  }

  private void executeRequest(final HttpUriRequest request, final String url) {

    final DefaultHttpClient client = new DefaultHttpClient();
    final HttpParams params = client.getParams();

    // Setting 15 second timeouts
    HttpConnectionParams.setConnectionTimeout(params, 15 * 1000);
    HttpConnectionParams.setSoTimeout(params, 15 * 1000);

    HttpResponse httpResponse;

    try {
      httpResponse = client.execute(request);
      responseCode = httpResponse.getStatusLine().getStatusCode();
      message = httpResponse.getStatusLine().getReasonPhrase();

      final HttpEntity entity = httpResponse.getEntity();

      if (entity != null) {

        final InputStream instream = entity.getContent();
        response = convertStreamToString(instream);

        // Closing the input stream will trigger connection release
        instream.close();
      }

    } catch (final ClientProtocolException e) {
      client.getConnectionManager().shutdown();
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "RestClient - " + "223");
    } catch (final IOException e) {
      client.getConnectionManager().shutdown();
      FlurryAgent.onError("Catch", e.getMessage(), "RestClient - " + "226");
      e.printStackTrace();
    }
  }

  private static String convertStreamToString(final InputStream is) {

    final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    final StringBuilder sb = new StringBuilder();

    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }
    } catch (final IOException e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "RestClient - " + "243");
    } finally {
      try {
        is.close();
      } catch (final IOException e) {
        e.printStackTrace();
        FlurryAgent.onError("Catch", e.getMessage(), "RestClient - " + "249");
      }
    }
    return sb.toString();
  }
}