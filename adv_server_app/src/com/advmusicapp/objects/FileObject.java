package com.advmusicapp.objects;

import java.io.FileDescriptor;

/**
 * Created with IntelliJ IDEA.
 * User: vladya
 * Date: 2/14/13
 * Time: 2:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class FileObject {

    private FileDescriptor fd;

    public FileObject(){}

    public FileObject(FileDescriptor fd){
        this.fd = fd;
    }

    public FileDescriptor getFd() {
        return fd;
    }

    public void setFd(FileDescriptor fd) {
        this.fd = fd;
    }
}
