package com.advmusicapp.objects;

public class JsonAndNameContainer {
	public String name;
	public String json;
	
	public JsonAndNameContainer(String name, String json)
	{
		this.name = name;
		this.json = json;
	}
}
