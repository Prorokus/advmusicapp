package com.advmusicapp.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 11.02.13 Time: 23:36 To change this template use File
 * | Settings | File Templates.
 */
public class ClientData {
  private String uuid;
  private String city;
  private String name;
  private List<String> audioFiles;
  private HashMap<String, ArrayList<String>> audioFilesTimeToPlay;

  private String startTime;
  private String stopTime;
  private int songStatus; // (-1) - nothing 0 - stop 1 - start
  private int volume;
  private String nowPlaying;

  private String failedToPlayFiles = "";

  private boolean dontPlayFlag = false;

  public ClientData() {
  }

  // public ClientData(Context context) {
  // TelephonyManager tManager = (TelephonyManager) context
  // .getSystemService(Context.TELEPHONY_SERVICE);
  // uuid = tManager.getDeviceId();
  // city = "";
  // name = "";
  // audioFiles = new ArrayList<String>();
  // audioFilesTimeToPlay = new HashMap<String, List<String>>();
  // startTime = "12:00";
  // stopTime = "12:10";
  // songStatus = -1;
  // volume = 50;
  // nowPlaying = "";
  // }

  public boolean getDontPlayFlag ()
  {
    return dontPlayFlag;
  }

  public void setDontPlayFlag (final boolean b)
  {
    dontPlayFlag = b;
  }

  public String getFailedToPlayFiles()
  {
    return failedToPlayFiles;
  }

  public String getUuid() {
    return uuid;
  }

  public String getCity() {
    return city;
  }

  public void setCity(final String city) {
    this.city = city;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public List<String> getAudioFiles() {
    final List<String> l = new ArrayList<String>();

    for (final String s : this.audioFiles) {
      l.add(s);
    }

    return l;
  }

  public void setAudioFiles(final List<String> audioFiles) {
    this.audioFiles.clear();

    for (final String s : audioFiles) {
      this.audioFiles.add(s);
    }
  }

  public void setAudioFilesTimeToPlay(final HashMap<String, ArrayList<String>> audioFilesTimeToPlay) {
    this.audioFilesTimeToPlay = audioFilesTimeToPlay;
  }

  public HashMap<String, ArrayList<String>> getaudioFilesTimeToPlay() {
    if (audioFilesTimeToPlay == null) {
      audioFilesTimeToPlay = new HashMap<String, ArrayList<String>>();
    }

    return audioFilesTimeToPlay;
  }

  public String getStartTime() {
    return startTime;
  }

  public void setStartTime(final String startTime) {
    this.startTime = startTime;
  }

  public String getStopTime() {
    return stopTime;
  }

  public void setStopTime(final String stopTime) {
    this.stopTime = stopTime;
  }

  public int getSongStatus() {
    return songStatus;
  }

  public void setSongStatus(final int songStatus) {
    this.songStatus = songStatus;
  }

  public int getVolume() {
    return volume;
  }

  public void setVolume(final int volume) {
    this.volume = volume;
  }

  public String getNowPlaying() {
    return nowPlaying;
  }

  public void setNowPlaying(final String nowPlaying) {
    this.nowPlaying = nowPlaying;
  }

  /**
   * Returns client data converted into JSON format
   *
   */
  public JSONObject getJSON() {
    final JSONObject json = new JSONObject();
    try {
      json.put("uuid", UUID.randomUUID().toString());

      json.put("city", city);
      json.put("name", name);
      json.put("audioFiles", new JSONArray(audioFiles));

      if (audioFilesTimeToPlay != null) {
        json.put("audioFilesTimeToPlay", new JSONObject(audioFilesTimeToPlay));
      }
      json.put("startTime", startTime);
      json.put("stopTime", stopTime);
      json.put("songStatus", songStatus);
      json.put("volume", volume);
      json.put("nowPlaying", nowPlaying);
      json.put("dontPlayFlag", dontPlayFlag);
    } catch (final JSONException e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "144");
    }

    return json;
  }

  /**
   * Update client data from JSON received from web server
   *
   */
  public boolean updateFromJson(final JSONObject json) {
    try {
      if (!json.isNull("uuid")) {
        this.uuid = json.getString("uuid");
      }
      if (!json.isNull("city")) {
        this.city = json.getString("city");
      }
      if (!json.isNull("name")) {
        this.name = json.getString("name");
      }
      if (!json.isNull("audioFiles")) {
        final JSONArray jsonArray = json.getJSONArray("audioFiles");
        this.audioFiles = new ArrayList<String>();
        if (jsonArray != null) {
          final int len = jsonArray.length();
          for (int i = 0; i < len; i++) {
            this.audioFiles.add(jsonArray.get(i).toString());
          }
        }
      }

      if (!json.isNull("audioFilesTimeToPlay")) {
        final JSONObject jsonObj = json.getJSONObject("audioFilesTimeToPlay");
        this.audioFilesTimeToPlay = new HashMap<String, ArrayList<String>>();
        if (jsonObj != null) {
          final Iterator<?> keys = jsonObj.keys();

          while (keys.hasNext()) {
            final String key = (String) keys.next();
            final String mStringArray = jsonObj.getString(key).substring(1,
                jsonObj.getString(key).length() - 1);
            final Scanner s = new Scanner(mStringArray);
            s.useDelimiter(", ");
            final ArrayList<String> list = new ArrayList<String>();
            while (s.hasNext()) {
              list.add(s.next());
            }
            this.audioFilesTimeToPlay.put(key, list);

          }
        }
      }

      if (!json.isNull("startTime")) {
        this.startTime = json.getString("startTime");
      } else {
        this.startTime = "12:00";
      }
      if (!json.isNull("stopTime")) {
        this.stopTime = json.getString("stopTime");
      } else {
        this.stopTime = "12:10";
      }
      if (!json.isNull("songStatus")) {
        this.songStatus = json.getInt("songStatus");
      } else {
        this.songStatus = -1;
      }
      if (!json.isNull("volume")) {
        this.volume = json.getInt("volume");
      } else {
        this.volume = 50;
      }
      if (!json.isNull("nowPlaying")) {
        this.nowPlaying = json.getString("nowPlaying");
      }

      if (!json.isNull("dontPlayFlag")) {
        this.dontPlayFlag = json.getBoolean("dontPlayFlag");
      }

    } catch (final JSONException e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "198");
      return false;
    }

    return true;
  }

  /**
   * Update now playing song data from JSON received from web server
   *
   */
  public boolean updateNowPlayingFromJson(final JSONObject json) {
    try {
      if (!json.isNull("volume")) {
        this.volume = json.getInt("volume");
      } else {
        this.volume = 50;
      }
      if (!json.isNull("nowPlaying")) {
        this.nowPlaying = json.getString("nowPlaying");
      }

      if (!json.isNull("failedToPlayFiles")) {
        this.failedToPlayFiles = json.getString("failedToPlayFiles");
      }

    } catch (final JSONException e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "219");
      return false;
    }

    return true;
  }

  /**
   * Used to compare received JSON and current data
   *
   * @param j1
   *          - received JSON to compare with
   * @return true - if they are same, false if not
   */
  public boolean compareJSONAndModel(final JSONObject j1) {
    String s = "";
    boolean res = true;

    if (j1 == null) {
      return false;
    }

    try {
      s = j1.getString("uuid");
      if (!s.equals(this.uuid)) {
        res = false;
      }

      s = j1.getString("city");
      if (!s.equals(this.city)) {
        res = false;
      }

      s = j1.getString("name");
      if (!s.equals(this.name)) {
        res = false;
      }

      final JSONArray jArr = j1.getJSONArray("audioFiles");
      if (jArr != null) {
        final int len = jArr.length();

        if (len == this.audioFiles.size()) {
          for (int i = 0; i < len; i++) {
            if (!jArr.get(i).toString().equals(this.audioFiles.get(i))) {
              res = false;
            }
          }
        }
      }

      // final JSONObject jObj = j1.getJSONObject("audioFilesTimeToPlay");
      // if (jObj != null) {
      // final int len = jObj.length();
      // if (len == ClientData.audioFilesTimeToPlay.size()) {
      // final Iterator keys = jObj.keys();
      // while (keys.hasNext()) {
      // final String key = (String) keys.next();
      // final List<String> value = (List<String>) jObj.get(key);
      // if (!jObj.get(key).equals(ClientData.audioFilesTimeToPlay.get(key))) {
      // res = false;
      // }
      // }
      // for (int i = 0; i < len; i++) {
      // if (!jArr.get(i).toString().equals(this.audioFiles.get(i))) {
      // res = false;
      // }
      // }
      // }
      // }

      s = j1.getString("startTime");
      if (!s.equals(this.startTime)) {
        res = false;
      }

      s = j1.getString("stopTime");
      if (!s.equals(this.stopTime)) {
        res = false;
      }

      int i = j1.getInt("songStatus");
      if (i != this.songStatus) {
        res = false;
      }

      i = j1.getInt("volume");
      if (i != this.volume) {
        res = false;
      }

      s = j1.getString("nowPlaying");
      if (!s.equals(this.nowPlaying)) {
        res = false;
      }

    } catch (final JSONException e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "302");
    }

    return res;
  }

  /**
   * Generate new ClientData object from JSON
   *
   */
  public static ClientData fromJSON(final JSONObject json) {
    final ClientData cData = new ClientData();

    try {
      if (!json.isNull("uuid")) {
        cData.uuid = json.getString("uuid");
      }
      if (!json.isNull("city")) {
        cData.city = json.getString("city");
      }
      if (!json.isNull("name")) {
        cData.name = json.getString("name");
      }
      if (!json.isNull("audioFiles")) {
        final JSONArray jsonArray = json.getJSONArray("audioFiles");
        cData.audioFiles = new ArrayList<String>();
        if (jsonArray != null) {
          final int len = jsonArray.length();
          for (int i = 0; i < len; i++) {
            cData.audioFiles.add(jsonArray.get(i).toString());
          }
        }
      }

      if (!json.isNull("audioFilesTimeToPlay")) {
        final JSONObject jsonObj = json.getJSONObject("audioFilesTimeToPlay");
        cData.audioFilesTimeToPlay = new HashMap<String, ArrayList<String>>();
        if (jsonObj != null) {
          final Iterator<?> keys = jsonObj.keys();

          while (keys.hasNext()) {
            final String key = (String) keys.next();
            final String mStringArray = jsonObj.getString(key).substring(1,
                jsonObj.getString(key).length() - 1);
            final Scanner s = new Scanner(mStringArray);
            s.useDelimiter(", ");
            final ArrayList<String> list = new ArrayList<String>();
            while (s.hasNext()) {
              list.add(s.next());
            }
            cData.audioFilesTimeToPlay.put(key, list);

          }
        }
      }

      if (!json.isNull("startTime")) {
        cData.startTime = json.getString("startTime");
      } else {
        cData.startTime = "12:00";
      }
      if (!json.isNull("stopTime")) {
        cData.stopTime = json.getString("stopTime");
      } else {
        cData.stopTime = "12:10";
      }
      // if(!json.isNull("songStatus")){
      // cData.songStatus = json.getInt("songStatus");
      // }else{
      cData.songStatus = -1;
      // }
      if (!json.isNull("volume")) {
        cData.volume = json.getInt("volume");
      } else {
        cData.volume = 50;
      }
      if (!json.isNull("nowPlaying")) {
        cData.nowPlaying = json.getString("nowPlaying");
      }

      if (!json.isNull("dontPlayFlag")) {
        cData.dontPlayFlag = json.getBoolean("dontPlayFlag");
      }

    } catch (final JSONException e) {
      e.printStackTrace();
      FlurryAgent.onError("Catch", e.getMessage(), "ClientData - " + "357");
    }

    return cData;
  }

  @Override
  public String toString() {
    return name;
  }

}
