package com.advmusicapp.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.advmusicapp.MainServerActivity;
import com.advmusicapp.helpers.Const;
import com.advmusicapp.objects.ClientData;
import com.advmusicapp.objects.JsonAndNameContainer;
import com.advmusicapp.serverS.R;

/**
 * Created with IntelliJ IDEA.
 * User: O_o
 * Date: 10.02.13
 * Time: 16:08
 * To change this template use File | Settings | File Templates.
 */
public class CityFragment extends Fragment {

    public static CityFragment instance;
    private String currentCity;

    private View view;

    private Spinner cities;
    private ListView users;
    private Button playAll;
    private Button stopAll;

    private CityAdapter cityAdapter;
    private UsersAdapter usersAdapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup group, final Bundle saved)
    {
        view = inflater.inflate(R.layout.city_frag, group, false);
        return view;
    }
    @Override
    public void onActivityCreated (final Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        cities = (Spinner)view.findViewById(R.id.cities);
        users = (ListView)view.findViewById(R.id.users);
        playAll = (Button)view.findViewById(R.id.play_all);
        stopAll = (Button)view.findViewById(R.id.stop_all);

        cityAdapter = new CityAdapter();
        cities.setAdapter(cityAdapter);
        cities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int i, final long l) {
                updateUsersAdapter();
            }

            @Override
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        });

        usersAdapter = new UsersAdapter();
        users.setAdapter(usersAdapter);
        users.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l) {
                Const.selectedClient = Const.mCitiesAndNamesMap.get(currentCity).get(i);

                ((MainServerActivity)getActivity()).goToAudioFragment();
            }
        });
        users.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        initButtons();

        instance = this;
    }

    private void initButtons(){
    	//Pack all config files to ArrayList and send them in turn to web server
    	//setting client.setSongStatus(1); to start playing this config on client
        playAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
            	final ArrayList <JsonAndNameContainer> l = new ArrayList <JsonAndNameContainer>();

                for (final Map.Entry<String,ArrayList<ClientData>> entry : Const.mCitiesAndNamesMap.entrySet()) {
                    for(final ClientData client : entry.getValue()){
                        client.setSongStatus(1);

                        l.add(new JsonAndNameContainer(client.getName(), client.getJSON().toString()));

                        client.setSongStatus(-1); //to be sure that it doesn't play again after sending new values
                    }
                }

                Const.mDropBoxHelper.startWriteTask(l);
            }
        });

    	//Pack all config files to ArrayList and send them in turn to web server
    	//setting client.setSongStatus(0); to stop playing this config on client
        stopAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
            	final ArrayList <JsonAndNameContainer> l = new ArrayList <JsonAndNameContainer>();

                for (final Map.Entry<String,ArrayList<ClientData>> entry : Const.mCitiesAndNamesMap.entrySet()) {
                    for(final ClientData client : entry.getValue()){
                        client.setSongStatus(0);

                        l.add(new JsonAndNameContainer(client.getName(), client.getJSON().toString()));

                        client.setSongStatus(-1); //to be sure that it doesn't play again after sending new values
                    }
                }

                Const.mDropBoxHelper.startWriteTask(l);
            }
        });
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        instance = null;

        currentCity = null;
        usersAdapter = null;
    }

    private void updateCitiesAdapter(){
        cityAdapter.notifyDataSetChanged();
    }

    private void updateUsersAdapter(){
        currentCity = (String) cities.getSelectedItem();
        usersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume(){
        super.onResume();

        updateAllListsAndAdapters();
    }

    @Override
    public void onPause(){
        super.onPause();

    }

    /**
     * Ask Cities and Users adapter to update their data from model
     *
     */
    public void updateAllListsAndAdapters(){
        updateCitiesAdapter();
        updateUsersAdapter();
    }

    private class CityAdapter extends BaseAdapter implements SpinnerAdapter{

        List<String> citiesNames = new ArrayList<String>();

        public CityAdapter() {
            citiesNames = new ArrayList<String>(Const.mCitiesAndNamesMap.keySet());
        }

        @Override
        public int getCount() {
            return citiesNames.size();
        }

        @Override
        public String getItem(final int i) {
            return citiesNames.get(i);
        }

        @Override
        public long getItemId(final int i) {
            return i;
        }

        @Override
        public void notifyDataSetChanged(){
            citiesNames = new ArrayList<String>(Const.mCitiesAndNamesMap.keySet());

            if(citiesNames.size() > 0 && currentCity == null){
                currentCity = citiesNames.get(0);
                updateUsersAdapter();
            }
            super.notifyDataSetChanged();
        }

        @Override
        public View getView(final int i, final View convertView, final ViewGroup viewGroup) {
            View spinView;
            if( convertView == null ){
                final LayoutInflater inflater = getActivity().getLayoutInflater();
                spinView = inflater.inflate(android.R.layout.simple_spinner_item, null);
            } else {
                spinView = convertView;
            }
            final TextView t1 = (TextView) spinView.findViewById(android.R.id.text1);
            t1.setText(getItem(i));
            return spinView;
        }
    }

    private class UsersAdapter extends BaseAdapter{

        List<ClientData> usersNames = new ArrayList<ClientData>();

        public UsersAdapter(){
            getData();
        }

        private void getData(){
            if(Const.mCitiesAndNamesMap.keySet().size() > 0
                    && Const.mCitiesAndNamesMap.containsKey(currentCity)){
                usersNames = Const.mCitiesAndNamesMap.get(currentCity);

                if(Const.selectedClient == null){
                    Const.selectedClient = usersNames.get(0);
                }
            }
        }

        @Override
        public int getCount() {
            return usersNames.size();
        }

        @Override
        public ClientData getItem(final int i) {
            return usersNames.get(i);
        }

        @Override
        public long getItemId(final int i) {
            return i;
        }

        @Override
        public void notifyDataSetChanged(){
            getData();
            super.notifyDataSetChanged();
        }

        @Override
        public View getView(final int i, final View convertView, final ViewGroup viewGroup) {

            View textView;
            if( convertView == null ){
                final LayoutInflater inflater = getActivity().getLayoutInflater();
                textView = inflater.inflate(android.R.layout.simple_list_item_1, null);
            } else {
                textView = convertView;
            }
            final TextView t1 = (TextView) textView.findViewById(android.R.id.text1);

            final ClientData data = getItem(i);

            if(Const.selectedClient != null && data.equals(Const.selectedClient)){
                t1.setTextColor(Color.RED);
            }
            else {
                t1.setTextColor(Color.WHITE);
            }

            t1.setText(data.getName());

            return textView;
        }
    }
}
