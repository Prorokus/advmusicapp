package com.advmusicapp.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TimePicker;

import com.advmusicapp.helpers.Const;
import com.advmusicapp.objects.JsonAndNameContainer;
import com.advmusicapp.serverS.R;

/**
 * Created with IntelliJ IDEA.
 * User: O_o
 * Date: 10.02.13
 * Time: 16:08
 * To change this template use File | Settings | File Templates.
 */
public class AdditionalFragment extends Fragment {

    private View view;

    private TimePicker startTime;
    private TimePicker stopTime;
    private Button apply;

    private CheckBox dontPlayCheckBox;

    private boolean started;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup group, final Bundle saved)
    {
        view = inflater.inflate(R.layout.additional_frag, group, false);
        return view;
    }
    @Override
    public void onActivityCreated (final Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        startTime = (TimePicker)view.findViewById(R.id.start_time_picker);
        stopTime = (TimePicker)view.findViewById(R.id.stop_time_picker);

        dontPlayCheckBox = (CheckBox) view.findViewById(R.id.dontPlayCheckBox);

        apply = (Button)view.findViewById(R.id.apply);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if(Const.selectedClient != null){
                	startTime.clearFocus();
                	stopTime.clearFocus();

                    int hour = startTime.getCurrentHour();
                    int min = startTime.getCurrentMinute();

                    //Update start/stop time for selected client
                    Const.selectedClient.setStartTime(String.format("%d:%d",hour,min));
                    hour = stopTime.getCurrentHour();
                    min = stopTime.getCurrentMinute();
                    Const.selectedClient.setStopTime(String.format("%d:%d",hour,min));

                    Const.selectedClient.setDontPlayFlag(dontPlayCheckBox.isChecked());

                	final ArrayList <JsonAndNameContainer> l = new ArrayList <JsonAndNameContainer>();

                	//Prepare container with info to send on web server
                	l.add(new JsonAndNameContainer(Const.selectedClient.getName(), Const.selectedClient.getJSON().toString()));

                	//Send new data
                	Const.mDropBoxHelper.startWriteTask(l);

                	Const.selectedClient.setSongStatus(-1); //to be sure that it doesn't play again after sending new values
                }
            }
        });

        startTime.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(final TimePicker timePicker, final int i, final int i2) {
                if(!started) {
                  return;
                }

                final int hour = timePicker.getCurrentHour();
                final int min = timePicker.getCurrentMinute();
                Const.selectedClient.setStartTime(String.format("%d:%d",hour,min));
            }
        });

        stopTime.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(final TimePicker timePicker, final int i, final int i2) {
                if(!started) {
                  return;
                }

                final int hour = timePicker.getCurrentHour();
                final int min = timePicker.getCurrentMinute();
                Const.selectedClient.setStopTime(String.format("%d:%d",hour,min));
            }
        });

        dontPlayCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
            Const.selectedClient.setDontPlayFlag(isChecked);
          }
        });
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        started = false;
    }

    @Override
    public void onResume(){
        super.onResume();

        startTime.setIs24HourView(true);
        stopTime.setIs24HourView(true);

        if(Const.selectedClient != null){

            final int[] timeStart = parseTime(Const.selectedClient.getStartTime());
            startTime.setCurrentHour(timeStart[0]);
            startTime.setCurrentMinute(timeStart[1]);

            final int[] timeStop = parseTime(Const.selectedClient.getStopTime());
            stopTime.setCurrentHour(timeStop[0]);
            stopTime.setCurrentMinute(timeStop[1]);

            dontPlayCheckBox.setChecked(Const.selectedClient.getDontPlayFlag());
        }

        started = true;
    }

    private int[] parseTime(final String time){

        if(TextUtils.isEmpty(time)){
            return new int[]{12, 0};
        }

        final String[] parsedTime = time.split(":");
        final int hour = Integer.valueOf(parsedTime[0]);
        final int minute = Integer.valueOf(parsedTime[1]);

        return new int[]{hour, minute};
    }
}
