package com.advmusicapp.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.advmusicapp.SchedulersActivity;
import com.advmusicapp.helpers.Const;
import com.advmusicapp.objects.JsonAndNameContainer;
import com.advmusicapp.serverS.R;
import com.mobeta.android.dslv.DragSortListView;

/**
 * Created with IntelliJ IDEA. User: O_o Date: 10.02.13 Time: 16:08 To change this template use File
 * | Settings | File Templates.
 */
public class AudioFragment extends Fragment {

  public static AudioFragment instance;

  private View view;

  private Button clearSongs;
  private Button addSongs;
  private Button addAllSongs;

  private Button play;
  private Button apply;
  private Button stop;
  private SeekBar volume;
  private TextView volumeLvl;
  private TextView nowPlaying;
  private TextView failedSongs;

  public CustomListAdapter adapter;

  private DragSortListView userSongs;

  private ListView songsList;
  private static ArrayList<String> selectedSongs = new ArrayList<String>();

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup group, final Bundle saved) {
    view = inflater.inflate(R.layout.songs_frag, group, false);

    return view;
  }

  @Override
  public void onActivityCreated(final Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    userSongs = (DragSortListView) view.findViewById(R.id.user_songs);

    songsList = (ListView) view.findViewById(R.id.songs_list);
    songsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(final AdapterView<?> parent, final View item, final int position,
          final long id) {

        final CheckedTextView textView = (CheckedTextView) item;
        textView.setChecked(!textView.isChecked());

        final String songName = (String) parent.getAdapter().getItem(position);

        if (textView.isChecked()) {
          selectedSongs.add(songName);
        } else {
          selectedSongs.remove(songName);
        }
      }
    });

    volumeLvl = (TextView) view.findViewById(R.id.volume_txt_value);
    nowPlaying = (TextView) view.findViewById(R.id.now_playing);
    failedSongs = (TextView) view.findViewById(R.id.failedSongs);

    updateNowPlayingText();

    volume = (SeekBar) view.findViewById(R.id.volume);

    play = (Button) view.findViewById(R.id.play);
    stop = (Button) view.findViewById(R.id.stop);

    clearSongs = (Button) view.findViewById(R.id.clear_songs);
    addSongs = (Button) view.findViewById(R.id.add_songs);
    addAllSongs = (Button) view.findViewById(R.id.add_all);

    play.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        Const.selectedClient.setSongStatus(1); // start
      }
    });

    stop.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        Const.selectedClient.setSongStatus(0); // stop
      }
    });

    apply = (Button) view.findViewById(R.id.apply);
    apply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        if (Const.selectedClient != null) {
          final ArrayList<JsonAndNameContainer> jsonAndNameContainers = new ArrayList<JsonAndNameContainer>();

          final JsonAndNameContainer jsonAndNameContainer = new JsonAndNameContainer(
              Const.selectedClient.getName(), Const.selectedClient.getJSON().toString());
          jsonAndNameContainers.add(jsonAndNameContainer);

          Const.mDropBoxHelper.startWriteTask(jsonAndNameContainers);

          Const.selectedClient.setSongStatus(-1); // to be sure that
          // it doesn't
          // play again after
          // sending
          // new values
        }

      }
    });

    clearSongs.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        showSonsList("clear");
      }
    });

    addSongs.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        showSonsList("add");
      }
    });

    addAllSongs.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        showSonsList("all");
      }
    });

    if (Const.selectedClient == null)
    {
      getFragmentManager().popBackStackImmediate();
      return;
    }

    final int  volLvl = Const.selectedClient.getVolume();
    volumeLvl.setText("" + volLvl);

    volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

      @Override
      public void onProgressChanged(final SeekBar arg0, final int arg1, final boolean arg2) {
        volumeLvl.setText(Integer.toString(arg1));
        Const.selectedClient.setVolume(arg1);
      }

      @Override
      public void onStartTrackingTouch(final SeekBar arg0) {
      }

      @Override
      public void onStopTrackingTouch(final SeekBar arg0) {
      }
    });

    adapter = new CustomListAdapter(getActivity(), R.layout.list_item_handle_left, Const.selectedClient.getAudioFiles());

    userSongs.setAdapter(adapter);

    userSongs.setDropListener(onDrop);

    userSongs.setClickable(true);
    userSongs.setLongClickable(true);
    userSongs.setOnItemLongClickListener(onItemLongClick);

    userSongs.setChoiceMode(ListView.CHOICE_MODE_NONE);

    volume.setMax(100);
    volume.postDelayed(new Runnable() {
      @Override
      public void run() {
        volume.setProgress(volLvl);
      }
    }, 100);

    instance = this;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    instance = null;
  }

  /**
   * Update current song text label
   *
   */
  private void updateNowPlayingText() {
    if (Const.selectedClient == null)
    {
      return;
    }

    final String songName = Const.selectedClient.getNowPlaying();

    if (!TextUtils.isEmpty(songName)) {
      nowPlaying.setText("Playing: " + songName);
      failedSongs.setText(Const.selectedClient.getFailedToPlayFiles());
    } else {
      nowPlaying.setText("Stopped!");
      failedSongs.setText(Const.selectedClient.getFailedToPlayFiles());
    }
  }

  /**
   * Update current song text label and volume level (called after current song data received from
   * web server)
   *
   */
  public void updateData() {
    updateNowPlayingText();

    volume.postDelayed(new Runnable() {
      @Override
      public void run() {
        volume.setProgress(Const.selectedClient.getVolume());
      }
    }, 100);

    /*
     * if(selectedSongs != null && selectedSongs.size() > 0 &&
     * !Const.selectedClient.getAudioFiles().equals(selectedSongs)){
     * Const.selectedClient.setAudioFiles(selectedSongs); }
     */
  }

  /**
   * Open all songs list (program read their names from SD card - AdvMusic/audio folder) to edit
   * current client play list
   *
   * @param command
   *          - clear (clear selected songs and start new selection), add (start new selection to
   *          add songs to current song-list), all (auto select all songs to be added)
   */
  private void showSonsList(final String command) {
    final List<String> names = Const.mDropBoxHelper.getAudioFilesInfo();

    selectedSongs.clear();
    selectedSongs = (ArrayList<String>) Const.selectedClient.getAudioFiles();

    if (command.equals("all")) {
      for (final String s : names) {
        if (!selectedSongs.contains(s)) {
          selectedSongs.add(s);
        }
      }
    }

    final ArrayAdapter<String> selectSongsAdapter = new ArrayAdapter<String>(getActivity(),
        android.R.layout.simple_list_item_multiple_choice, android.R.id.text1, names) {
      @Override
      public View getView(final int i, final View convertView, final ViewGroup viewGroup) {

        View view;
        final String songName = getItem(i);

        if (convertView == null) {
          final LayoutInflater inflater = getActivity().getLayoutInflater();
          view = inflater.inflate(android.R.layout.simple_list_item_multiple_choice, null);
        } else {
          view = convertView;
        }

        final CheckedTextView ctv = (CheckedTextView) view.findViewById(android.R.id.text1);
        ctv.setText(songName);

        if (selectedSongs.contains(songName)) {
          ctv.setChecked(true);
        } else {
          ctv.setChecked(false);
        }

        return view;
      }
    };
    songsList.setAdapter(selectSongsAdapter);
    songsList.setVisibility(View.VISIBLE);

    if (command.equals("clear")) {
      selectedSongs.clear();
    }
  }

  /**
   * Hide selection songs list and add all selected songs as new current play list
   *
   */
  public boolean hideSongsList() {
    if (songsList.getVisibility() == View.VISIBLE) {
      songsList.setVisibility(View.GONE);
      Const.selectedClient.setAudioFiles(selectedSongs);

      adapter = new CustomListAdapter(getActivity(), R.layout.list_item_radio,
          Const.selectedClient.getAudioFiles());
      userSongs.setAdapter(adapter);

      return true;
    }

    return false;
  }

  private final OnItemLongClickListener onItemLongClick = new OnItemLongClickListener() {
    @Override
    public boolean onItemLongClick(final AdapterView<?> av, final View v, final int pos,
        final long id) {

      final Intent intent = new Intent(getActivity(), SchedulersActivity.class);
      intent.putExtra("songName", String.valueOf(adapter.getItem(pos)));
      startActivity(intent);

      Toast.makeText(getActivity(), "Long Press work", Toast.LENGTH_SHORT).show();
      return true;
    }
  };

  /**
   * Allows to sort songs in play list by dragging them (tap on the picture left of the song and
   * then drag)
   *
   */
  private final DragSortListView.DropListener onDrop = new DragSortListView.DropListener() {
    @Override
    public void drop(final int from, final int to) {
      if (from != to) {

        final String item = String.valueOf(adapter.getItem(from));
        adapter.remove(item);
        adapter.insert(item, to);

        final ArrayList<String> l = new ArrayList<String>();
        for (int i = 0; i < adapter.getCount(); i++) {
          l.add(adapter.getItem(i).toString());
        }

        Const.selectedClient.setAudioFiles(l);

        // userSongs.moveCheckState(from, to);
        // Log.d("DSLV", "Selected item is " +
        // userSongs.getCheckedItemPosition());
      }
    }
  };

  public class CustomListAdapter extends ArrayAdapter<String> {

    private final Context mContext;
    private final int id;
    private final List<String> items;

    public CustomListAdapter(final Context context, final int textViewResourceId,
        final List<String> list) {
      super(context, textViewResourceId, list);
      mContext = context;
      id = textViewResourceId;
      items = list;
    }

    @Override
    public View getView(final int position, final View v, final ViewGroup parent) {
      View mView = v;
      if (mView == null) {
        final LayoutInflater vi = (LayoutInflater) mContext
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = vi.inflate(id, null);
      }

      final TextView text = (TextView) mView.findViewById(R.id.text);

      final ArrayList<String> timers = Const.selectedClient.getaudioFilesTimeToPlay().get(
          items.get(position));

      if (items.get(position) != null) {
        if (timers != null && !timers.isEmpty()) {

          if (timers.size() > 1)
          {
            text.setText(items.get(position) + " - " + timers.size() + " ��������");
          }
          else
          {
            text.setText(items.get(position) + " - " + timers.size() + " ������");
          }

          text.setBackgroundColor(Color.RED);
          final int color = Color.argb(200, 255, 64, 64);
          text.setBackgroundColor(color);
        } else {
          text.setText(items.get(position));
          text.setBackgroundColor(Color.TRANSPARENT);
        }

      }

      return mView;
    }
  }
}
