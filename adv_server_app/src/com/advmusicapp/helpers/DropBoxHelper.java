package com.advmusicapp.helpers;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.advmusicapp.objects.JsonAndNameContainer;
import com.advmusicapp.serverS.R;
import com.flurry.android.FlurryAgent;

/**
 * DropBox API was here in past. Now songs synchronization is managed by
 * dropSync program. And config files synchronization is managed by web server.
 *
 * This class is just helps to have conversation with CommunicationHelper. Also this class
 * checks for config files updates on web server
 *
 * @author Oleg Lytovchenko
 */

public class DropBoxHelper {
    private static FragmentActivity parrentActivity;

    public ProgressDialog mProgressDialog;
    public static Handler mHandler;

    private final Timer updateTimer = new Timer();
    private TimerTask tS;

    //All config files names list
    private String sdDevicesDirList;

    //Changed nowPlaying info file names (used to know which clients is needed to be updated with new )
    private String sdNowPlayingDirList;

    //List with ALL audio file names (used for song selection list)
    //private File[] sdAudioDirList;
    private String sdAudioDirList;

    private List <String> oldConfigFilesInfo = new ArrayList<String>();

    public DropBoxHelper(final FragmentActivity act)
    {
    	parrentActivity = act;

    	mHandler = new Handler();

    	mProgressDialog = new ProgressDialog(parrentActivity);
    	mProgressDialog.setCancelable(false);

    	new ListRootFolderTask().execute();

        //Init timer to check new config data on web server every 35 seconds
		tS = new TimerTask() {
			@Override
			public void run() {
				updateNowPlaying();
			}
		};

    	updateTimer.schedule(tS, 35 * 1000);
	}

    /**
     * Reset timer when exiting program
     *
     */
    public void kill()
    {
    	tS.cancel();
    	mProgressDialog.dismiss();
    }

    /**
     * Generate md5 string from city name
     * (all config and nowPlaying files are stored under md5 names at web server)
     * this md5 strings generated from current client names in every city
     */
    public static String getMd5FromString(final String s)
    {
    	 MessageDigest m = null;

    	 try {
			m = MessageDigest.getInstance("MD5");
	    	m.update(s.getBytes(), 0, s.length());
	    	return new BigInteger(1, m.digest()).toString(16);
		} catch (final NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FlurryAgent.onError("Catch", e.getMessage(), "DropBoxHelper - " + "80");
		}

    	return "";
    }

    /**
     * Send check new data event to main activity
     * and restart timer
     *
     */
    private void updateNowPlaying()
    {
		tS = new TimerTask() {
			@Override
			public void run() {
				updateNowPlaying();
			}
		};

		updateTimer.purge();
		updateTimer.schedule(tS, 35 * 1000);

		new UpdateDataFolderTask().execute();
    }

    public void showOkBox()
	{
		/*AlertDialog.Builder dlgAlert = new AlertDialog.Builder(parrentActivity);

		dlgAlert.setMessage(parrentActivity.getString(R.string.info_sent));
		dlgAlert.setTitle(parrentActivity.getString(R.string.app_name));
		dlgAlert.setPositiveButton("OK", null);
		dlgAlert.setCancelable(true);
		dlgAlert.create().show();

		dlgAlert.setPositiveButton("Ok",
			    new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) {
		         //dismiss the dialog
			        }
			    });*/
	}

	public static void showToastMsg(final String msg)
	{
		mHandler.post(new Runnable() {
			@Override
      public void run() {
				Toast.makeText(parrentActivity, msg, Toast.LENGTH_SHORT).show();
			}
		});
	}

    /**
     * Write config file for current client on web server
     *
     * @param name - client name
     * @param inf - JSON client data converted to String
     */
	public void writeConfigFile(final String name, final String inf)
	{
		String md5Name = "";

		md5Name = getMd5FromString(name);

		CommunicationHelper.WriteConfigFile(md5Name, inf.toString());
	}

    /**
     * Read config file from server
     *
     * @param name - current client name
     * @return JSON with asked file
     */
	public JSONObject readConfigFile (String name)
	{
		if (name.endsWith(".conf"))
		{
			name = name.replace(".conf", "");
		}

		String aBuffer = CommunicationHelper.readConfigFile(name);

		aBuffer = aBuffer.replace("\\", "");

	    JSONObject j = null;

	    if (!aBuffer.equals(""))
	    {
			try {
				j = new JSONObject (aBuffer);
			} catch (final JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FlurryAgent.onError("Catch", e.getMessage(), "DropBoxHelper - " + "157");
			}
	    }

	    return j;
	}

    /**
     * Read file with current playing songs from web server
     *
     * @param name - current client name
     * @return JSON with asked file
     */
	public JSONObject readNowPlayingFile (String name)
	{
		if (name.endsWith(".conf"))
		{
			name = name.replace(".conf", "");
		}

		String aBuffer = CommunicationHelper.readNowPLayingFile(getMd5FromString(name));

		aBuffer = aBuffer.replace("\\", "");

	    JSONObject j = null;

	    if (!aBuffer.equals(""))
	    {
			try {
				j = new JSONObject (aBuffer);
			} catch (final JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				FlurryAgent.onError("Catch", e.getMessage(), "DropBoxHelper - " + "184");
			}
	    }

	    return j;
	}

    /**
     * Remove extension from file name (for example ololo.mp3 = ololo)
     *
     */
	public static String removeExtention(final File file) {
	    String name = file.getName();

	    // Now we know it's a file - don't need to do any special hidden
	    // checking or contains() checking because of:
	    final int lastPeriodPos = name.lastIndexOf('.');

	    if (lastPeriodPos <= 0)
	    {
	        // No period after first character - return name as it was passed in
	        return name;
	    }
	    else
	    {
	        // Remove the last period and everything after it
	        name = name.substring(0, lastPeriodPos);
	        return name;
	    }
	}

    /**
     * Parce array with files received from file system
     * to receive List with their names without extensions
     *
     * @return List<String> with file names (parsed sdAudioDirList)
     */
    public synchronized List<String> getAudioFilesInfo ()
    {
      /*final List<String> res = new ArrayList<String>();

      if (this.sdAudioDirList == null)
      {
        return res;
      }

      for (final File inf : this.sdAudioDirList)
      {
        res.add(removeExtention(inf));
      }

      return res;*/
    	List<String> res = new ArrayList<String>();

    	if (this.sdAudioDirList.equals(""))
    	{
    		return res;
    	}

    	String strToParse = new String (this.sdAudioDirList);

      while (strToParse.length() > 0)
      {
        int indexStart = strToParse.indexOf("audio\\/");
        int indexEnd = strToParse.indexOf("audio\\/", indexStart + 1);

        if (indexStart == -1)
        {
          res = fixUnicodeNames(res);
          return res;
        }
        else
        {
          indexStart += 6;
        }

        if (indexEnd == -1)
        {
          indexEnd = strToParse.length() - 3;
        }
        else
        {
          indexEnd -= 3;
        }

        res.add(strToParse.substring(indexStart + 1, indexEnd));

        strToParse = strToParse.substring(indexEnd, strToParse.length());
      }

      res = fixUnicodeNames(res);
    	return res;
    }

    private List<String> fixUnicodeNames(final List<String> paramList)
    {
      final List<String> resList = new ArrayList<String>();

      for (String s : paramList)
      {
        if (s.contains("\\u"))
        {
          //res.remove(s);
          String newS = "";

          while (s.length() > 0)
          {
            if (!s.startsWith("\\"))
            {
              newS += s.substring(0, 1);
              s = s.substring(1, s.length());
            }
            else
            {
              s = s.substring(2, s.length());
              final String convert = s.substring(0, 4);
              s = s.substring(4, s.length());
              final int hexVal = Integer.parseInt(convert, 16);
              newS += (char)hexVal;
            }
          }

          resList.add(newS);
        }
        else {
          resList.add(s);
        }
      }

      return resList;
    }

    /**
     * Parce String that contains list of config files names
     * (for example - ["devices\/eab71244afb687f16d8c4f5ee9d6ef0e.conf"] - only 1 name)
     *
     * @return List<String> with file names (eab71244afb687f16d8c4f5ee9d6ef0e only) (parsed sdDevicesDirList)
     */
    public synchronized List<String> getConfigFilesInfo ()
    {
    	String strToParse = this.sdDevicesDirList;
    	final ArrayList <String> lRes = new ArrayList <String> ();

    	while (strToParse.length() > 0)
    	{
    		final int index = strToParse.indexOf("/");

    		if (index == -1)
    		{
    			return lRes;
    		}

    		strToParse = strToParse.substring(index + 1, strToParse.length());

    		final String strToAdd = strToParse.substring(0, 32);

    		lRes.add(strToAdd);

    		strToParse = strToParse.substring(33, strToParse.length());
    	}

    	return lRes;
    }

    /**
     * Parce String that contains list of nowPlaying info files names
     * (for example - ["playing\/eab71244afb687f16d8c4f5ee9d6ef0e.conf"] - only 1 name)
     *
     * @return List<String> with file names (eab71244afb687f16d8c4f5ee9d6ef0e only) (parsed sdNowPlayingDirList)
     */
    public synchronized ArrayList <String> getPlayingFilesInfo ()
    {
    	String strToParse = this.sdNowPlayingDirList;
    	final ArrayList <String> lRes = new ArrayList <String> ();

    	while (strToParse.length() > 0)
    	{
    		final int index = strToParse.indexOf("/");

    		if (index == -1)
    		{
    			return lRes;
    		}

    		strToParse = strToParse.substring(index + 1, strToParse.length());

    		final String strToAdd = strToParse.substring(0, 32);

    		lRes.add(strToAdd);

    		strToParse = strToParse.substring(33, strToParse.length());
    	}

    	return lRes;
    }

    /**
     * Perform all apply operations (sending data to web server)
     * in the separate thread
     *
     */
    @SuppressWarnings("unchecked")
	public void startWriteTask(final ArrayList <JsonAndNameContainer> l)
    {
    	new WriteConfigTask().execute(l);
    }

    public class WriteConfigTask extends AsyncTask<ArrayList <JsonAndNameContainer>, Void, Integer> {

        @Override
        protected void onPreExecute() {
       mHandler.post(new Runnable() {
				@Override
        public void run() {
				  if (parrentActivity != null && !parrentActivity.isFinishing())
				  {
				    mProgressDialog.setMessage("Sending Data");
				    mProgressDialog.show();
				  }
				}
			});
        }

        @Override
        protected Integer doInBackground(final ArrayList <JsonAndNameContainer>... params) {
        	for (final JsonAndNameContainer jC : params[0])
        	{
        		writeConfigFile(jC.name, jC.json);
        	}

            return null;
        }

        @Override
        protected void onPostExecute(final Integer result) {
			mHandler.post(new Runnable() {
				@Override
        public void run() {
				  if (parrentActivity != null && !parrentActivity.isFinishing())
				  {
  				  if (mProgressDialog.isShowing()) {
  		            	mProgressDialog.hide();
  		      }
				  }
				}
			});
        }
    }

    /**
     * Check if any of config files were added/deleted
     * Update all songs list
     * Check for nowPlaying info modified files
     *
     */
    public class UpdateDataFolderTask extends AsyncTask<String, Void, Void>
    {
		ArrayList <String> deletedConfigFiles = new ArrayList<String>();
		ArrayList <String> addedConfigFiles = new ArrayList<String>();

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected synchronized Void doInBackground(final String... strings)
        {
          //List with ALL audio file names
          //final File myFile = new File(Environment.getExternalStorageDirectory() + "/AdvMusic/audio/");
        	//sdAudioDirList = myFile.listFiles();
          sdAudioDirList = CommunicationHelper.getAllSongsList();

        	//Changed nowPlaying info file names
        	sdNowPlayingDirList = CommunicationHelper.getAllChangedNowPLayingList();

        	//List of all config files names
        	sdDevicesDirList = CommunicationHelper.getAllConfigsList();

        	final List <String> newConfigFilesInfo = getConfigFilesInfo();

        	for (final String s : oldConfigFilesInfo)
        	{
        		if (!newConfigFilesInfo.contains(s))
        		{
        			deletedConfigFiles.add(s);
        		}
        	}

        	for (final String s : newConfigFilesInfo)
        	{
        		if (!oldConfigFilesInfo.contains(s))
        		{
        			addedConfigFiles.add(s);
        		}
        	}

        	oldConfigFilesInfo = getConfigFilesInfo();

        	return null;
        }

        @Override
        protected void onPostExecute(final Void v) {
        	//There are some DELETED configs
        	//we need to send this event to Main activity
        	if (deletedConfigFiles != null && deletedConfigFiles.size() > 0)
        	{
		    	final Intent intent = new Intent();
		    	intent.setAction("com.advmusicapp.configFilesRemoved");
		    	intent.putStringArrayListExtra("data", deletedConfigFiles);
		    	parrentActivity.sendBroadcast(intent);
        	}

        	//There are some ADDED configs
        	//we need to send this event to Main activity
        	if (addedConfigFiles != null && addedConfigFiles.size() > 0)
        	{
		    	final Intent intent = new Intent();
		    	intent.setAction("com.advmusicapp.configFilesAdded");
		    	intent.putStringArrayListExtra("data", addedConfigFiles);
		    	parrentActivity.sendBroadcast(intent);
        	}

        	//There are some changed nowPlaying info files
        	//we need to send this event to Main activity
        	if (sdNowPlayingDirList != null && sdNowPlayingDirList.length() > 0)
        	{
		    	final Intent intent = new Intent();
		    	intent.setAction("com.advmusicapp.nowPlayingFilesModified");
		    	intent.putStringArrayListExtra("data", getPlayingFilesInfo());
		    	parrentActivity.sendBroadcast(intent);
        	}

        	showToastMsg("Updated");
        }
    }

    /**
     *Called after Main activity start. Get all songs list (from sd card).
     *Get all config files names from web server.
     *Launch event to main activity (it will ask web server to send all clients info)
     *
     */
    public class ListRootFolderTask extends AsyncTask<String, Void, Void>
    {
        @Override
        protected void onPreExecute() {
			mHandler.post(new Runnable() {
				@Override
        public void run() {
				  if (parrentActivity != null && !parrentActivity.isFinishing())
				  {
				    mProgressDialog.setMessage(parrentActivity.getString(R.string.receiving_files_list));
		        mProgressDialog.show();
				  }
				}
			});
        }

        @Override
        protected synchronized Void doInBackground(final String... strings)
        {
			mHandler.post(new Runnable() {
				@Override
        public void run() {
				  if (parrentActivity != null && !parrentActivity.isFinishing())
				  {
				  mProgressDialog.setMessage("����� ������ �����");
				  }
				}
			});

			//final File myFile = new File(Environment.getExternalStorageDirectory() + "/AdvMusic/audio/");
			//sdAudioDirList = myFile.listFiles();
			sdAudioDirList = CommunicationHelper.getAllSongsList();

			mHandler.post(new Runnable() {
				@Override
        public void run() {
				  if (parrentActivity != null && !parrentActivity.isFinishing())
				  {
		        	mProgressDialog.setMessage("������� ������ �����");
				  }
				}
			});

			sdDevicesDirList = CommunicationHelper.getAllConfigsList();
			oldConfigFilesInfo = getConfigFilesInfo();

			return null;
        }

        @Override
        protected void onPostExecute(final Void v) {
        	final Intent intent = new Intent();
	    	intent.setAction("com.advmusicapp.filesSynchedOnStart");
	    	parrentActivity.sendBroadcast(intent);
        }
    }
}
