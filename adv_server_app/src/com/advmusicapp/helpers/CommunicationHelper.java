package com.advmusicapp.helpers;

import com.flurry.android.FlurryAgent;
import com.tylersmith.net.RequestMethod;
import com.tylersmith.net.RestClient;


/**
 * Static functions that are responsible for all communications with the server
 *
 * @author Oleg Lytovchenko
 */

public class CommunicationHelper {

    private static String WRITE_CONFIG_URL = "http://sog1.ru/shtender/set/json/devices";
    private static String WRITE_PLAYING_URL = "http://sog1.ru/shtender/set/json/playing";

    private static String READ_CONFIG_URL = "http://sog1.ru/shtender/get/json/devices";
    private static String READ_PLAYING_URL = "http://sog1.ru/shtender/get/json/playing";

    private static String GET_CONFIGS_LIST_URL = "http://sog1.ru/shtender/get/files/devices";
    private static String GET_SONGS_LIST_URL = "http://sog1.ru/shtender/get/songs/audio";
    private static String GET_PLAYING_LIST_URL = "http://sog1.ru/shtender/get/files/playing";

    private static String GET_NEW_CONFIG_URL = "http://sog1.ru/shtender/new/json/devices";
    private static String GET_NEW_PLAYING_URL = "http://sog1.ru/shtender/new/json/playing";

    private static String GET_NEW_CONFIG_LIST_URL = "http://sog1.ru/shtender/new/files/devices";
    private static String GET_NEW_PLAYING_LIST_URL = "http://sog1.ru/shtender/new/files/playing";

    /**
     * Get base RestClient
     *
     * @param url url for sending data
     * @return RestClient
     * */
    private static RestClient getCustomClient(final String url)
    {
        final RestClient restClient = new RestClient(url);

        return restClient;
    }

    /**
     * Send config file on web server
     *
     * @param name - client name converted in md5
     * @param json - client data
     * @return true if no errors
     */
    public static boolean WriteConfigFile(final String name, final String json)
    {
        final RestClient restClient = getCustomClient(WRITE_CONFIG_URL);

        restClient.addParam("name", name);
        restClient.addParam("json", json);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return true;
            }

        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "74");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return false;
    }

    /**
     * Send now playing info file on web server
     *
     * @param name - client name converted in md5
     * @param json - client data
     * @return true if no errors
     */
    public static boolean WriteNowPLayingFile(final String name, final String json)
    {
        final RestClient restClient = getCustomClient(WRITE_PLAYING_URL);

        restClient.addParam("name", name);
        restClient.addParam("json", json);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return true;
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "97");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return false;
    }

    /**
     * Get config file from web server
     *
     * @param name - client name converted in md5
     * @return JSON if success, empty string if error
     */
    public static String readConfigFile(final String name)
    {
        final RestClient restClient = getCustomClient(READ_CONFIG_URL);

        restClient.addParam("name", name);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "119");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get now playing music info file from web server
     *
     * @param name - client name converted in md5
     * @return JSON if success, empty string if error
     */
    public static String readNowPLayingFile(final String name)
    {
        final RestClient restClient = getCustomClient(READ_PLAYING_URL);

        restClient.addParam("name", name);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "141");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get all config file names (md5) list from web server
     *
     * @return JSON if success, empty string if error
     */
    public static String getAllConfigsList()
    {
        final RestClient restClient = getCustomClient(GET_CONFIGS_LIST_URL);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "161");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get all config file names (md5) list from web server
     *
     * @return JSON if success, empty string if error
     */
    public static String getAllSongsList()
    {
        final RestClient restClient = getCustomClient(GET_SONGS_LIST_URL);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "204");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get all now playing song info files names (md5) list from web server
     *
     * @return JSON if success, empty string if error
     */
    public static String getAllNowPLayingList()
    {
        final RestClient restClient = getCustomClient(GET_PLAYING_LIST_URL);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "181");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get config file JSON from web server, if it was changed from last attempt to get it
     *
     * @param name - client name converted in md5
     * @return JSON if success, empty string if error or if it was'nt changed
     */
    public static String getChangedConfigFile(final String name)
    {
        final RestClient restClient = getCustomClient(GET_NEW_CONFIG_URL);

        restClient.addParam("name", name);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "203");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get now playing info JSON from web server, if it was changed from last attempt to get it
     *
     * @param name - client name converted in md5
     * @return JSON if success, empty string if error or if it was'nt changed
     */
    public static String getChangedNowPlayingFile(final String name)
    {
        final RestClient restClient = getCustomClient(GET_NEW_PLAYING_URL);

        restClient.addParam("name", name);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "225");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get JSON list of file names (md5) of config files from web server (files that was changed from last attempt to getting them)
     *
     * @return JSON if success, empty string if error or if no any file changed
     */
    public static String getAllChangedConfigsList()
    {
        final RestClient restClient = getCustomClient(GET_NEW_CONFIG_LIST_URL);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "244");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }

    /**
     * Get JSON list of file names (md5) of now playing info files from web server (files that was changed from last attempt to getting them)
     *
     * @return JSON if success, empty string if error or if no any file changed
     */
    public static String getAllChangedNowPLayingList()
    {
        final RestClient restClient = getCustomClient(GET_NEW_PLAYING_LIST_URL);

        try {
            restClient.execute(RequestMethod.POST);
            if(restClient.getResponseCode() == 200) {
                //Successfully connected
                return restClient.getResponse();
            }
        } catch (final Exception e) {
            e.printStackTrace();
            FlurryAgent.onError("Catch", e.getMessage(), "CommunicationHelper - " + "265");
        }

        DropBoxHelper.showToastMsg("Error sending data");

        return "";
    }
}
