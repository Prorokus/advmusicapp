package com.advmusicapp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.ListActivity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.advmusicapp.helpers.Const;
import com.advmusicapp.serverS.R;

public class SchedulersActivity extends ListActivity {

  private ListView listView;
  private ArrayAdapter<String> adapter;
  private int minute;
  private int hour;
  protected Object key;
  // protected HashMap<String, ArrayList<String>> audioFilesTimeToPlay;
  private String songName;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    listView = getListView();
    final LayoutInflater inflater = SchedulersActivity.this.getLayoutInflater();
    final RelativeLayout listFooterView = (RelativeLayout) inflater.inflate(
        R.layout.schedulers_footer, null);

    listView.addFooterView(listFooterView);
    listView.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(final AdapterView<?> myAdapter, final View myView,
          final int position, final long mylng) {

        adapter.remove(adapter.getItem(position));
        adapter.notifyDataSetChanged();
        updateMap();

      }
    });

    // audioFilesTimeToPlay = new HashMap<String, ArrayList<String>>();
    final Intent intent = getIntent();
    songName = intent.getStringExtra("songName");

    adapter = new ArrayAdapter<String>(SchedulersActivity.this, R.layout.schedulers, R.id.timers);
    final List<String> audioFilesTimers = Const.selectedClient.getaudioFilesTimeToPlay().get(
        songName);

    if (audioFilesTimers != null) {
      for (final String audioFileTimer : audioFilesTimers) {
        adapter.add(audioFileTimer);
      }
    }
    setListAdapter(adapter);
    super.onCreate(savedInstanceState);
  }

  public void addNewTimer(final View v) {
    final Calendar c = Calendar.getInstance();
    hour = c.get(Calendar.HOUR_OF_DAY);
    minute = c.get(Calendar.MINUTE);

    final TimePickerDialog timerDialog = new TimePickerDialog(this, myDateSetListener, hour,
        minute, true);
    timerDialog.show();

  }

  private void updateMap() {
    final ArrayList<String> underlying = new ArrayList<String>();
    for (int i = 0; i < adapter.getCount(); i++) {
      underlying.add(adapter.getItem(i));
    }

    // audioFilesTimeToPlay.put(songName, underlying);
    Const.selectedClient.getaudioFilesTimeToPlay().put(songName, underlying);
  }

  OnTimeSetListener myDateSetListener = new OnTimeSetListener() {

    @Override
    public void onTimeSet(final TimePicker timePicker, final int hour, final int minute) {

      final String timeInString = formatValue(hour) + ":" + formatValue(minute);

      //Check for duplicates
      for (int i = 0; i < adapter.getCount(); i++) {
        if (adapter.getItem(i).equals(timeInString))
        {
          return;
        }
      }

      adapter.add(timeInString);

      updateMap();
    }

    private String formatValue(final int value) {
      if (value <= 9) {
        return String.valueOf("0" + value);
      } else {
        return String.valueOf(value);
      }
    }
  };
}
