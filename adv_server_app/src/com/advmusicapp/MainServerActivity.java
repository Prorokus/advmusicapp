package com.advmusicapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TabHost;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.advmusicapp.fragments.AdditionalFragment;
import com.advmusicapp.fragments.AudioFragment;
import com.advmusicapp.fragments.CityFragment;
import com.advmusicapp.helpers.Const;
import com.advmusicapp.helpers.DropBoxHelper;
import com.advmusicapp.objects.ClientData;
import com.advmusicapp.serverS.R;
import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;

/**
 * Created with IntelliJ IDEA.
 * User: O_o
 * Date: 10.02.13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
public class MainServerActivity extends SherlockFragmentActivity {

    //private PowerManager.WakeLock wl;

    private TabHost mTabHost;
    private ActionBar actionBar;

    private TabManager mTabManager;

    private ConfigFilesAddedReceiver mConfigFilesAddedReceiver;
    private ConfigFilesRemovedReceiver mConfigFilesRemovedReceiver;

    private NowPlayingFilesModifiedReceiver mNowPlayingFilesModifiedReceiver;

    private FilesSynchedOnStartReceiver mFilesSynchedOnStartReceiver;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Ensure one instance of your main activity
        if (!isTaskRoot()) {
          final Intent intent = getIntent();
          final String intentAction = intent.getAction();
          if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) && intentAction != null
              && intentAction.equals(Intent.ACTION_MAIN)) {
            finish();
          }
        }

        Crashlytics.start(this);
        final KeyguardManager mKeyGuardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        final KeyguardLock mLock = mKeyGuardManager.newKeyguardLock("adv_server_app");
        mLock.disableKeyguard();

        android.provider.Settings.System.putInt(getContentResolver(), android.provider.Settings.System.WIFI_SLEEP_POLICY, android.provider.Settings.System.WIFI_SLEEP_POLICY_NEVER);

        //PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        //wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "ADVLOCK");

        final IntentFilter filterConfigFilesAdded = new IntentFilter("com.advmusicapp.configFilesAdded");
        mConfigFilesAddedReceiver = new ConfigFilesAddedReceiver();
        this.registerReceiver(mConfigFilesAddedReceiver, filterConfigFilesAdded);

        final IntentFilter filterConfigFilesRemoved = new IntentFilter("com.advmusicapp.configFilesRemoved");
        mConfigFilesRemovedReceiver = new ConfigFilesRemovedReceiver();
        this.registerReceiver(mConfigFilesRemovedReceiver, filterConfigFilesRemoved);

        final IntentFilter filterNowPlayingFilesModified = new IntentFilter("com.advmusicapp.nowPlayingFilesModified");
        mNowPlayingFilesModifiedReceiver = new NowPlayingFilesModifiedReceiver();
        this.registerReceiver(mNowPlayingFilesModifiedReceiver, filterNowPlayingFilesModified);

        final IntentFilter filesSynchedOnStart = new IntentFilter("com.advmusicapp.filesSynchedOnStart");
        mFilesSynchedOnStartReceiver = new FilesSynchedOnStartReceiver();
        this.registerReceiver(mFilesSynchedOnStartReceiver, filesSynchedOnStart);

        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.hide();

        setContentView(R.layout.fragment_tabs);
        mTabHost = (TabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup();

        mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);

        mTabManager.addTab(mTabHost.newTabSpec("city").setIndicator("City"),
                CityFragment.class, null);
        mTabManager.addTab(mTabHost.newTabSpec("audio").setIndicator("Audio"),
                AudioFragment.class, null);
        mTabManager.addTab(mTabHost.newTabSpec("additional").setIndicator("Additional"),
                AdditionalFragment.class, null);

        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
        }

        Const.mCitiesAndNamesMap.clear();
        Const.mDropBoxHelper = new DropBoxHelper(this);

        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tab", mTabHost.getCurrentTabTag());
    }

    @Override
    protected void onPause() {
        super.onPause();
        //wl.release();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (AudioFragment.instance != null && AudioFragment.instance.adapter != null)
        {
          AudioFragment.instance.adapter.notifyDataSetChanged();
        }
        //wl.acquire();
    }

    @Override
    protected void onStart()
    {
    	super.onStart();
    	FlurryAgent.onStartSession(this, "GYQHWVPY5FQ6D2CK7NM5");
    }

    @Override
    protected void onStop()
    {
    	super.onStop();
    	FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onDestroy()
    {
    	this.unregisterReceiver(mConfigFilesAddedReceiver);
    	this.unregisterReceiver(mConfigFilesRemovedReceiver);

    	this.unregisterReceiver(mNowPlayingFilesModifiedReceiver);

        this.unregisterReceiver(mFilesSynchedOnStartReceiver);

        if(Const.mDropBoxHelper != null) {
          Const.mDropBoxHelper.kill();
        }

    	super.onDestroy();
    }

    public void goToAudioFragment(){
        mTabHost.setCurrentTab(1);
    }

    /**
     * Perform all networking operations in the separate thread
     *
     */
    class addCitiesAndNamesMapTask extends AsyncTask<List <String>, Void, Void>
    {
    	@Override
      protected Void doInBackground(final List <String>... params)
    	{
			addCitiesAndNamesMap(params[0], true);

			 DropBoxHelper.mHandler.post(new Runnable() {
				@Override
        public void run() {
					updateFragmentsData();
				}
			});

			return null;
    	}

        @Override
        protected void onPostExecute(final Void v) {
        	DropBoxHelper.mHandler.post(new Runnable() {
				@Override
        public void run() {
		        	if (Const.mDropBoxHelper.mProgressDialog.isShowing()) {
		        		Const.mDropBoxHelper.mProgressDialog.hide();
		            }
				}
			});
        }
     }

    /**
     * Perform all networking operations in the separate thread
     *
     */
    class removeCitiesAndNamesMapTask extends AsyncTask<List <String>, Void, Void>
    {
    	@Override
      protected Void doInBackground(final List <String>... params)
    	{
    		removeCitiesAndNamesMap(params[0], true);

			 DropBoxHelper.mHandler.post(new Runnable() {
				@Override
        public void run() {
					updateFragmentsData();
				}
			});

			return null;
    	}
     }

    /**
     * Perform all networking operations in the separate thread
     *
     */
    class modifyNowPlayingInfoTask extends AsyncTask<List <String>, Void, Void>
    {
    	@Override
      protected Void doInBackground(final List <String>... params)
    	{
    		modifyNowPlayingInfo(params[0]);

			 DropBoxHelper.mHandler.post(new Runnable() {
				@Override
        public void run() {
					updateFragmentsData();
				}
			});

			return null;
    	}
     }

    private class ConfigFilesAddedReceiver extends BroadcastReceiver
    {
    	 @Override
    	 public void onReceive(final Context arg0, final Intent arg1) {
    		 new addCitiesAndNamesMapTask().execute(arg1.getStringArrayListExtra("data"));
    	 }
    }

    private class ConfigFilesRemovedReceiver extends BroadcastReceiver
    {
    	 @Override
    	 public void onReceive(final Context arg0, final Intent arg1) {
    		 new removeCitiesAndNamesMapTask().execute(arg1.getStringArrayListExtra("data"));
    	 }
    }

    private class NowPlayingFilesModifiedReceiver extends BroadcastReceiver
    {
    	 @Override
    	 public void onReceive(final Context arg0, final Intent arg1) {
    		 new modifyNowPlayingInfoTask().execute(arg1.getStringArrayListExtra("data"));
    	 }
    }

    private class FilesSynchedOnStartReceiver extends BroadcastReceiver
    {
    	 @Override
    	 public void onReceive(final Context arg0, final Intent arg1) {
    		 new addCitiesAndNamesMapTask().execute(Const.mDropBoxHelper.getConfigFilesInfo());
    	 }
    }


    /**
     * Add new ClientData to mCitiesAndNamesMap if there are no
     * client with the same name
     *
     * @param city - used as key in mCitiesAndNamesMap
     * @param cD - data to add
     */
    private synchronized void addToCitiesMap(final String city, final ClientData cD)
    {
    	final ArrayList<ClientData> cDataList = Const.mCitiesAndNamesMap.get(city);

    	for (final ClientData cData : cDataList)
    	{
    		if (cData.getName().equals(cD.getName()))
    		{
    			return;
    		}
    	}

    	cDataList.add(cD);
    }

    /**
     * Add received new config files data to mCitiesAndNamesMap
     *
     * @param sList - (Generated by DropBoxHelper) list of config files to add to mCitiesAndNamesMap
     * @param showToast - show debug information in Toast (disabled)
     */
    private synchronized void addCitiesAndNamesMap(final List <String> sList, final boolean showToast)
    {
    	if (sList == null)
    	{
    		return;
    	}

    	String namesToAdd = "";

    	int interCount = 0;
    	final int sListSizeFinal = sList.size();

    	for (final String s : sList)
    	{
    		//Get data from web server
    		final JSONObject json = Const.mDropBoxHelper.readConfigFile(s);

    		if (json != null)
    		{
	    		String name = "null";
				try {
					name = json.getString("name");
				} catch (final JSONException e) {
					e.printStackTrace();
					FlurryAgent.onError("Catch", e.getMessage(), "MainServerActivity - " + "300");
				}

				interCount++;
				final int iterCountFinal = interCount;
				final String finalName = name;
				DropBoxHelper.mHandler.post(new Runnable() {
					@Override
          public void run() {
						Const.mDropBoxHelper.mProgressDialog.setMessage("������� � �������� ����� - '" + finalName + "'. �������� ����� " + iterCountFinal + " / " + sListSizeFinal);
					}
				});

	    		String city = "null";
				try {
					city = json.getString("city");
				} catch (final JSONException e) {
					FlurryAgent.onError("Catch", e.getMessage(), "MainServerActivity - " + "307");
					e.printStackTrace();
				}

	    		//Add new city name as key (if does not exist)
				if (!Const.mCitiesAndNamesMap.containsKey(city))
	    	{
				  /*String fixedCity = city;
				  if (fixedCity.startsWith(" ")) {
				    fixedCity = fixedCity.substring(1);
				  }*/

				  //if (Const.cityFilter.equals("All"))
				  //{
				    Const.mCitiesAndNamesMap.put(city, new ArrayList <ClientData>());
				  //}

				  /*else if (Const.cityFilter.equals("Volgograd") && (fixedCity.startsWith("V") || fixedCity.startsWith("v") || fixedCity.startsWith("W") || fixedCity.startsWith("w") || fixedCity.startsWith("�") || fixedCity.startsWith("�") || fixedCity.startsWith("B") || fixedCity.startsWith("b")))
				  {
				    Const.mCitiesAndNamesMap.put(city, new ArrayList <ClientData>());
				  }

				  else if (Const.cityFilter.equals("AstrahanVolgograd") && (fixedCity.startsWith("a") || fixedCity.startsWith("A") || fixedCity.startsWith("�") || fixedCity.startsWith("�") || fixedCity.startsWith("V") || fixedCity.startsWith("v") || fixedCity.startsWith("W") || fixedCity.startsWith("w") || fixedCity.startsWith("�") || fixedCity.startsWith("�") || fixedCity.startsWith("B") || fixedCity.startsWith("b")))
				  {
				    Const.mCitiesAndNamesMap.put(city, new ArrayList <ClientData>());
				  }

				  else
				  {
				    continue;
				  }*/
	    	}

	    		//Generate ClientData object from received JSON
				final ClientData cD = ClientData.fromJSON(json);
	    		cD.setNowPlaying("");

	    		//Update selectedClient link with newer created object
	    		if (Const.selectedClient != null && Const.selectedClient.getName().equals(cD.getName()))
	    		{
	    			Const.selectedClient = cD;
	    		}

	    		addToCitiesMap(city, cD);

	    		//Collect debug info
	    		namesToAdd = namesToAdd.concat(name + ", ");
    		}
    	}

    	/*if (showToast && sList.size() > 0)
    	{
    		Const.mDropBoxHelper.showToastMsg(getString(R.string.configs_received) + ": " + namesToAdd);
    	}
		else if (!showToast && sList.size() > 0)
		{
			Const.mDropBoxHelper.showToastMsg(getString(R.string.configs_modified) + ": " + namesToAdd);
		}*/
    }

    /**
     * Remove received new config files data from mCitiesAndNamesMap
     *
     * @param sList - (Generated by DropBoxHelper) list of config files to remove to mCitiesAndNamesMap
     * @param showToast - show debug information in Toast (disabled)
     */
    private synchronized void removeCitiesAndNamesMap(final List <String> sList, final boolean showToast)
    {
    	if (sList == null)
    	{
    		return;
    	}

		Iterator<Entry<String, ArrayList<ClientData>>> it1 = Const.mCitiesAndNamesMap.entrySet().iterator();
		String namesToRemove = "";

		//Search for client data to remove
		while (it1.hasNext())
		{
			final Entry<String, ArrayList<ClientData>> pairs = it1.next();

			int i = 0;
			while (i < pairs.getValue().size())
			{
				final ClientData cD = pairs.getValue().get(i);

				//Correct client name found, need to remove
				if (sList.contains(DropBoxHelper.getMd5FromString(cD.getName())))
				{
					//Collect debug info
					namesToRemove = namesToRemove.concat(cD.getName() + ", ");
					pairs.getValue().remove(cD);
				}
				else
				{
					i++;
				}
			}
		}

		it1 = Const.mCitiesAndNamesMap.entrySet().iterator();

		//Remove all empty cities (that has no any client)
		while (it1.hasNext())
		{
			final Entry<String, ArrayList<ClientData>> pairs = it1.next();

			if (pairs.getValue().size() == 0)
			{
				it1.remove();
			}
		}

		/*if (showToast && sList.size() > 0)
		{
			Const.mDropBoxHelper.showToastMsg(getString(R.string.configs_removed) + ": " + namesToRemove);
		}*/
    }

    /**
     * Remove received new config files data from mCitiesAndNamesMap
     *
     * @param sList - (Generated by DropBoxHelper) list of config files to remove to mCitiesAndNamesMap
     * @param showToast - show debug information in Toast (disabled)
     */
    private synchronized void modifyNowPlayingInfo(final List <String> sList)
    {
    	if (sList == null)
    	{
    		return;
    	}

    	final Iterator<Entry<String, ArrayList<ClientData>>> it1 = Const.mCitiesAndNamesMap.entrySet().iterator();
		String namesToModify = "";

		while (it1.hasNext())
		{
			final Entry<String, ArrayList<ClientData>> pairs = it1.next();

      /*String fixedCity = pairs.getKey();
      if (fixedCity.startsWith(" ")) {
        fixedCity = fixedCity.substring(1);
      }*/

      /*if (Const.cityFilter.equals("Volgograd") && !fixedCity.startsWith("V") && !fixedCity.startsWith("v") && !fixedCity.startsWith("W") && !fixedCity.startsWith("w") && !fixedCity.startsWith("�") && !fixedCity.startsWith("�") && !fixedCity.startsWith("B") && !fixedCity.startsWith("b"))
      {
        continue;
      }

      else if (Const.cityFilter.equals("AstrahanVolgograd") && !fixedCity.startsWith("a") && !fixedCity.startsWith("A") && !fixedCity.startsWith("�") && !fixedCity.startsWith("�") && !fixedCity.startsWith("V") && !fixedCity.startsWith("v") && !fixedCity.startsWith("W") && !fixedCity.startsWith("w") && !fixedCity.startsWith("�") && !fixedCity.startsWith("�") && !fixedCity.startsWith("B") && !fixedCity.startsWith("b"))
      {
        continue;
      }*/

			int i = 0;
			while (i < pairs.getValue().size())
			{
				final ClientData cD = pairs.getValue().get(i);

				if (sList.contains(DropBoxHelper.getMd5FromString(cD.getName())))
				{
					final JSONObject json = Const.mDropBoxHelper.readNowPlayingFile(cD.getName());
					if (json != null)
					{
						String n = "";
						try {
							n = json.getString("name");
						} catch (final JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							FlurryAgent.onError("Catch", e.getMessage(), "MainServerActivity - " + "419");
						}

						if (cD.updateNowPlayingFromJson(json))
						{
							namesToModify = namesToModify.concat(n + ", ");
						}
					}
				}

				i++;
			}
		}

		/*if (sList.size() > 0)
		{
			Const.mDropBoxHelper.showToastMsg(getString(R.string.configs_modified) + ": " + namesToModify);
		}*/
    }

    private void updateFragmentsData(){
        updateCitiesFragment();
        updateAudioFragment();
    }

    private void updateCitiesFragment(){

        if(CityFragment.instance != null)
        {
            CityFragment.instance.updateAllListsAndAdapters();
        }
    }

    private void updateAudioFragment(){

        if(AudioFragment.instance != null)
        {
            AudioFragment.instance.updateData();
        }
    }

    /**
     * This is a helper class that implements a generic mechanism for
     * associating fragments with the tabs in a tab host.  It relies on a
     * trick.  Normally a tab host has a simple API for supplying a View or
     * Intent that each tab will show.  This is not sufficient for switching
     * between fragments.  So instead we make the content part of the tab host
     * 0dp high (it is not shown) and the TabManager supplies its own dummy
     * view to show as the tab content.  It listens to changes in tabs, and takes
     * care of switch to the correct fragment shown in a separate content area
     * whenever the selected tab changes.
     */
    public static class TabManager implements TabHost.OnTabChangeListener {
        private final FragmentActivity mActivity;
        private final TabHost mTabHost;
        private final int mContainerId;
        private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
        TabInfo mLastTab;

        static final class TabInfo {
            private final String tag;
            private final Class<?> clss;
            private final Bundle args;
            private Fragment fragment;

            TabInfo(final String _tag, final Class<?> _class, final Bundle _args) {
                tag = _tag;
                clss = _class;
                args = _args;
            }
        }

        static class DummyTabFactory implements TabHost.TabContentFactory {
            private final Context mContext;

            public DummyTabFactory(final Context context) {
                mContext = context;
            }

            @Override
            public View createTabContent(final String tag) {
                final View v = new View(mContext);
                v.setMinimumWidth(0);
                v.setMinimumHeight(0);
                return v;
            }
        }

        public TabManager(final FragmentActivity activity, final TabHost tabHost, final int containerId) {
            mActivity = activity;
            mTabHost = tabHost;
            mContainerId = containerId;
            mTabHost.setOnTabChangedListener(this);
        }

        public void addTab(final TabHost.TabSpec tabSpec, final Class<?> clss, final Bundle args) {
            tabSpec.setContent(new DummyTabFactory(mActivity));
            final String tag = tabSpec.getTag();

            final TabInfo info = new TabInfo(tag, clss, args);

            // Check to see if we already have a fragment for this tab, probably
            // from a previously saved state.  If so, deactivate it, because our
            // initial state is that a tab isn't shown.
            info.fragment = mActivity.getSupportFragmentManager().findFragmentByTag(tag);
            if (info.fragment != null && !info.fragment.isDetached()) {
                final FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                ft.detach(info.fragment);
                ft.commit();
            }

            mTabs.put(tag, info);
            mTabHost.addTab(tabSpec);
        }

        @Override
        public void onTabChanged(final String tabId) {
            final TabInfo newTab = mTabs.get(tabId);
            if (mLastTab != newTab) {
                final FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
                if (mLastTab != null) {
                    if (mLastTab.fragment != null) {
                        ft.detach(mLastTab.fragment);
                    }
                }
                if (newTab != null) {
                    if (newTab.fragment == null) {
                        newTab.fragment = Fragment.instantiate(mActivity,
                                newTab.clss.getName(), newTab.args);
                        ft.add(mContainerId, newTab.fragment, newTab.tag);
                    } else {
                        ft.attach(newTab.fragment);
                    }
                }
                mLastTab = newTab;
                ft.commit();
                mActivity.getSupportFragmentManager().executePendingTransactions();
            }
        }
    }

    @Override
    public void onBackPressed(){
        boolean hide = false;

        if(AudioFragment.instance != null)
        {
            hide = AudioFragment.instance.hideSongsList();
        }

        if(!hide){
            super.onBackPressed();
        }
    }
}
